package com.creatah.inspireitsolutions.employee.app.modal;

import com.google.gson.annotations.SerializedName;

/**
 * Created by creatah1 on 07-08-2017.
 */

public class ExistingCustomerResponse extends BaseResponse {

    @SerializedName("customer_details")
    private Customer customerDetails;

    public Customer getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(Customer customerDetails) {
        this.customerDetails = customerDetails;
    }
}
