package com.creatah.inspireitsolutions.employee.app.manager;

import android.content.Context;
import android.text.TextUtils;

import com.android.framework.utils.Constants;
import com.android.framework.utils.ServiceConstants;
import com.android.framework.utils.Utils;
import com.android.network.beans.HttpVerb;
import com.android.network.beans.ServiceRequest;
import com.android.network.listener.ResponseListener;
import com.android.network.listener.ServiceResponseListener;
import com.android.network.manager.NetworkManager;
import com.creatah.inspireitsolutions.employee.app.modal.AddAMCServiceResponse;
import com.creatah.inspireitsolutions.employee.app.modal.AddCommentResponse;
import com.creatah.inspireitsolutions.employee.app.modal.AddGeneralServiceResponse;
import com.creatah.inspireitsolutions.employee.app.modal.AnnualServiceListResponse;
import com.creatah.inspireitsolutions.employee.app.modal.AppInfo;
import com.creatah.inspireitsolutions.employee.app.modal.CommentList;
import com.creatah.inspireitsolutions.employee.app.modal.EmployeeLocationResponse;
import com.creatah.inspireitsolutions.employee.app.modal.ExistingCustomerResponse;
import com.creatah.inspireitsolutions.employee.app.modal.ForgotPasswordResponse;
import com.creatah.inspireitsolutions.employee.app.modal.LoginResponse;
import com.creatah.inspireitsolutions.employee.app.modal.MemberListResponse;
import com.creatah.inspireitsolutions.employee.app.modal.ServiceListResponse;
import com.creatah.inspireitsolutions.employee.app.modal.SubscriptionValidationResponse;
import com.creatah.inspireitsolutions.employee.app.modal.UpdateFCMIdResponse;
import com.creatah.inspireitsolutions.employee.app.modal.UpdateServiceResponse;
import com.loopj.android.http.RequestParams;


public class ServiceManager {


    private static boolean isDummyService = false;


    //Get AppInfo
    public static void getAppInfo(Context context, RequestParams requestParams, final ServiceResponseListener<AppInfo> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.GET_APP_INFO);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, AppInfo.class, new ResponseListener<AppInfo>() {

                @Override
                public void onSuccess(AppInfo response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<AppInfo> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }

    }

    public static void doLogin(Context context, RequestParams requestParams, final ServiceResponseListener<LoginResponse> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.LOGIN);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, LoginResponse.class, new ResponseListener<LoginResponse>() {

                @Override
                public void onSuccess(LoginResponse response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<LoginResponse> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }
    }

    public static void getServiceList(Context context, RequestParams requestParams, final ServiceResponseListener<ServiceListResponse> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.SERVICE_LIST);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, ServiceListResponse.class, new ResponseListener<ServiceListResponse>() {

                @Override
                public void onSuccess(ServiceListResponse response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<ServiceListResponse> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }
    }


    public static void getMemberList(Context context, RequestParams requestParams, final ServiceResponseListener<MemberListResponse> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.MEMBER_LIST);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, MemberListResponse.class, new ResponseListener<MemberListResponse>() {

                @Override
                public void onSuccess(MemberListResponse response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<MemberListResponse> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }
    }


    public static void updateServiceStatus(Context context, RequestParams requestParams, final ServiceResponseListener<UpdateServiceResponse> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.UPDATE_SERVICE_STATUS);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, UpdateServiceResponse.class, new ResponseListener<UpdateServiceResponse>() {

                @Override
                public void onSuccess(UpdateServiceResponse response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<UpdateServiceResponse> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }
    }

    public static void sendFCMId(Context context, RequestParams requestParams, final ServiceResponseListener<UpdateFCMIdResponse> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.FCM_UPDATE);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, UpdateFCMIdResponse.class, new ResponseListener<UpdateFCMIdResponse>() {

                @Override
                public void onSuccess(UpdateFCMIdResponse response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<UpdateFCMIdResponse> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }
    }


    public static void getCommentList(Context context, RequestParams requestParams, final ServiceResponseListener<CommentList> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.COMMENT_LIST);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, CommentList.class, new ResponseListener<CommentList>() {

                @Override
                public void onSuccess(CommentList response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<CommentList> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }
    }


    //Add Comment
    public static void addComment(Context context, RequestParams requestParams, final ServiceResponseListener<AddCommentResponse> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.ADD_COMMENT);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, AddCommentResponse.class, new ResponseListener<AddCommentResponse>() {

                @Override
                public void onSuccess(AddCommentResponse response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<AddCommentResponse> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }
    }


    //Update employee location

    public static void updateEmployeeLocation(Context context, RequestParams requestParams, final ServiceResponseListener<EmployeeLocationResponse> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.UPDATE_EMPLOYEE_LOCATION);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, EmployeeLocationResponse.class, new ResponseListener<EmployeeLocationResponse>() {

                @Override
                public void onSuccess(EmployeeLocationResponse response) {
                    listener.onSuccess(response);
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    listener.onFailure(throwable, errorResponse);
                }

                @Override
                public Class<EmployeeLocationResponse> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), e.getMessage());
        }
    }


    //get annual service list
    public static void getAnnualServiceList(Context context, RequestParams requestParams, final ServiceResponseListener<AnnualServiceListResponse> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.ANNUAL_SERVICE_LIST);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, AnnualServiceListResponse.class, new ResponseListener<AnnualServiceListResponse>() {

                @Override
                public void onSuccess(AnnualServiceListResponse response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<AnnualServiceListResponse> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }
    }

    public static void forgotPassword(Context context, RequestParams requestParams, final ServiceResponseListener<ForgotPasswordResponse> listener) {
        String targetUrl;

        targetUrl = getServiceAPIUrl(ServiceConstants.FORGOT_PASSWORD);


        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, ForgotPasswordResponse.class, new ResponseListener<ForgotPasswordResponse>() {

                @Override
                public void onSuccess(ForgotPasswordResponse response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<ForgotPasswordResponse> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }
    }


    public static void checkExistingCustomer(Context context, RequestParams requestParams, final ServiceResponseListener<ExistingCustomerResponse> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.CHECK_EXISTING_CUSTOMER);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, ExistingCustomerResponse.class, new ResponseListener<ExistingCustomerResponse>() {

                @Override
                public void onSuccess(ExistingCustomerResponse response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<ExistingCustomerResponse> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }
    }


    public static void addGeneralService(Context context, RequestParams requestParams, final ServiceResponseListener<AddGeneralServiceResponse> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.ADD_GENERAL_SERVICE);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, AddGeneralServiceResponse.class, new ResponseListener<AddGeneralServiceResponse>() {

                @Override
                public void onSuccess(AddGeneralServiceResponse response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<AddGeneralServiceResponse> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }
    }

    public static void checkValidSubscriptionId(Context context, RequestParams requestParams, final ServiceResponseListener<SubscriptionValidationResponse> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.CHECK_VALID_SUBSCRIPTION);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, SubscriptionValidationResponse.class, new ResponseListener<SubscriptionValidationResponse>() {

                @Override
                public void onSuccess(SubscriptionValidationResponse response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<SubscriptionValidationResponse> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }
    }


    public static void addAMCService(Context context, RequestParams requestParams, final ServiceResponseListener<AddAMCServiceResponse> listener) {

        String targetUrl = getServiceAPIUrl(ServiceConstants.ADD_AMC_SERVICE);

        ServiceRequest request = new ServiceRequest.Builder(targetUrl).verb(HttpVerb.POST).addRequestParams(requestParams).build();
        try {
            NetworkManager.executeJsonAsync(context, request, AddAMCServiceResponse.class, new ResponseListener<AddAMCServiceResponse>() {

                @Override
                public void onSuccess(AddAMCServiceResponse response) {
                    if (listener != null) {
                        listener.onSuccess(response);
                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    if (listener != null) {
                        listener.onFailure(throwable, errorResponse);
                    }
                }

                @Override
                public Class<AddAMCServiceResponse> getTargetType() {
                    // TODO Auto-generated method stub
                    return null;
                }

                @Override
                public void onFinish() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onStart() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onSuccess(String arg0) {
                    // TODO Auto-generated method stub
                    listener.onSuccess(null);

                }
            });

        } catch (Exception e) {
            listener.onFailure(e.getCause(), "");
        }
    }


    //getting service url
    public static String getServiceAPIUrl(String key) {

        if (TextUtils.isEmpty(key)) {
            return Constants.EMPTY;
        }

        return Utils.getResponse().getdata().getApiservice().get(ServiceConstants.SERVER_URL_KEY) +
                Utils.getResponse().getdata().getApiservice().get(ServiceConstants.API_URL_ENDPOINT) +
                Utils.getResponse().getdata().getApiservice().get(key);

    }


}
