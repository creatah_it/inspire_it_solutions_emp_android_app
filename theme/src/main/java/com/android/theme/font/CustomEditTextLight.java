package com.android.theme.font;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.android.theme.R;

/**
 * Created by creatah1 on 08-06-2017.
 */

public class CustomEditTextLight  extends EditText {

    public CustomEditTextLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public CustomEditTextLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);

    }

    public CustomEditTextLight(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);
            String fontName = a.getString(R.styleable.MyTextView_fontName);
            if (fontName != null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
                setTypeface(myTypeface);
            }else {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + "Bauhaus_lt.ttf");
                setTypeface(myTypeface);
            }
            a.recycle();
        }
    }

}