package com.creatah.inspireitsolutions.employee.app.sqllite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "InspireItSolution";
    private static final String LocationListTableName = "EmployeeLocationList";

    //LocationListTableName Colum
    private static final String PRIMARY_ID = "emp_id";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String DATE = "date";
    private static final String TIME = "time";


    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE1 = "CREATE TABLE " + LocationListTableName + "("
                + PRIMARY_ID + " INTEGER PRIMARY KEY," + LATITUDE + " VARCHAR,"
                + LONGITUDE + " VARCHAR, " + DATE + " VARCHAR, " + TIME + " VARCHAR" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE1);

        Log.e("asd", "asd");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + LocationListTableName);
        // Creating tables again
        onCreate(db);
    }

    public void addLocation(Locationlist location) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LATITUDE, location.getLatitude()); // Locationlist
        values.put(LONGITUDE, location.getLongitude()); // Locationlist
        values.put(DATE, location.getDate()); // Locationlist
        values.put(TIME, location.getTime()); // Locationlist
        // Inserting Row
        db.insert(LocationListTableName, null, values);
        db.close(); // Closing database connection
    }


    public void deleteinsertedRow(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(LocationListTableName, PRIMARY_ID + "=" + id, null);
        Log.e("deleted","id");
        db.close(); // Closing database connection
    }

    public void deleteAllRow(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ LocationListTableName);
    }

    // Getting All Locations
    public List<Locationlist> getAllLocations() {
        List<Locationlist> locationlists = new ArrayList<Locationlist>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + LocationListTableName;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Locationlist locationlist = new Locationlist();
                locationlist.setId(Integer.parseInt(cursor.getString(0)));
                locationlist.setLatitude(cursor.getString(1));
                locationlist.setLongitude(cursor.getString(2));
                locationlist.setDate(cursor.getString(3));
                locationlist.setTime(cursor.getString(4));
                // Adding contact to list
                locationlists.add(locationlist);
            } while (cursor.moveToNext());
        }

        // return contact list
        return locationlists;
    }


}

