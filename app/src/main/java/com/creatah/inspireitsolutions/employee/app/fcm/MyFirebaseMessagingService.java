package com.creatah.inspireitsolutions.employee.app.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;

import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.activity.HomeActivity;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by creatah1 on 19-05-2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.d(TAG, "From: " + remoteMessage.getData().get("message"));
      //  Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        if(AppUtils.isAuthenticatedUserInfo(getApplication())) {
            sendNotification(remoteMessage);
        }

    }

    private void sendNotification(RemoteMessage remoteMessage) {
        int icon = R.drawable.app_icon;
        String timeStamp = new SimpleDateFormat("hh:mm a").format(Calendar.getInstance().getTime());
        Notification ss;
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent(this, HomeActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Notification notif;
            Notification.Builder notifBuilder = new Notification.Builder(getApplicationContext());
            notifBuilder.setContentIntent(contentIntent);
            notifBuilder.setContentTitle(getString(R.string.app_name));
            notifBuilder.setSmallIcon(R.drawable.status_icon);
            notifBuilder.setContentText(remoteMessage.getData().get("message"));
            notifBuilder.setDefaults(Notification.DEFAULT_ALL);
            notifBuilder.setAutoCancel(true);
            notifBuilder.setColor(getResources().getColor(R.color.colorAccent));
            //notifBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.appicon));
            notifBuilder.setWhen(System.currentTimeMillis());
            notif = notifBuilder.build();
            mNotificationManager.notify(2, notif);
        } else {
            ss = new Notification(R.drawable.status_icon1, "Custom Notification", System.currentTimeMillis());
            RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_notification);
            contentView.setImageViewResource(R.id.image, icon);
            contentView.setTextViewText(R.id.title, getString(R.string.app_name));
            contentView.setTextViewText(R.id.text, remoteMessage.getData().get("message"));
            contentView.setTextViewText(R.id.time, timeStamp);
            ss.contentView = contentView;
            ss.contentIntent = contentIntent;
            ss.flags |= Notification.FLAG_AUTO_CANCEL; //Do not clear the notification
            ss.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            ss.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
            mNotificationManager.notify(2, ss);
        }
    }
}