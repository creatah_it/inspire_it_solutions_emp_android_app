package com.android.framework.utils;

/**
 * Created by Sathish on 08/08/2015.
 */
public class ServiceConstants {

    public static final int SUCCESS_CODE = 200;
    public static final int FAILURE_CODE = 400;
    public static final int NETWORk_ERROR = 404;
    public static final String NETWORk_ERROR_STRING = "Network Error";
    public static final int LoginInvalid = 501;


    public static final String SERVER_URL_KEY = "serviceUrl";
    public static final String API_URL_ENDPOINT = "apiEnpoint";
    public static final String APP_CONFIG_ENDPOINT = "appConfig";
    public static final String IMAGE_ENDPOINT_URL = "imageEndpoint";
    public static final String CATEGORY_URL = "categoryListing";
    public static final String CLASSIFIED_URL = "classified";


    public static final String IMAGE_URL_ENDPOINT = "imageEndPoint";
    public static final String IMAGE_ICON_URL_ENDPOINT = "ORU_service_icons";

    public static final String GET_APP_INFO = "GetAppInfo";
    public static final String LOGIN = "UserLogin";
    public static final String SERVICE_LIST = "ServiceList";
    public static final String MEMBER_LIST = "MemberList";
    public static final String UPDATE_SERVICE_STATUS = "UpdateServiceStatus";
    public static final String FCM_UPDATE = "FCMUpdate";
    public static final String COMMENT_LIST = "CommentList";
    public static final String ADD_COMMENT = "AddComment";
    public static final String UPDATE_EMPLOYEE_LOCATION = "UpdateEmployeeLocation";
    public static final String ANNUAL_SERVICE_LIST = "AnnulServiceList";
    public static final String FORGOT_PASSWORD = "ForgotPassword";
    public static final String CHECK_EXISTING_CUSTOMER = "CheckExistingCustomer";
    public static final String ADD_GENERAL_SERVICE = "AddGeneralService";
    public static final String CHECK_VALID_SUBSCRIPTION = "CheckValidSubscription";
    public static final String ADD_AMC_SERVICE = "AddAMCService";
}
