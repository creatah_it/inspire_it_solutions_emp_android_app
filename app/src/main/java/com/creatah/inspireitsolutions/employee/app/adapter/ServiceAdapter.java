package com.creatah.inspireitsolutions.employee.app.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.activity.HomeActivity;
import com.creatah.inspireitsolutions.employee.app.activity.ServiceCommentActivity;
import com.creatah.inspireitsolutions.employee.app.activity.ServiceDetailsActivity;
import com.creatah.inspireitsolutions.employee.app.fragment.ServiceFragment;
import com.creatah.inspireitsolutions.employee.app.modal.InspireService;
import com.creatah.inspireitsolutions.employee.app.utils.AppConstant;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;

import java.util.List;

/**
 * Created by creatah1 on 03-06-2017.
 */

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceViewHolder> {

    private Activity activity;
    private List<InspireService> serviceList;
    private OnServiceListener listener;

    public ServiceAdapter(Activity activity, List<InspireService> serviceList, OnServiceListener listener) {
        this.activity = activity;
        this.serviceList = serviceList;
        this.listener = listener;
    }

    @Override
    public ServiceAdapter.ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_layout_service, parent, false);
        ServiceViewHolder viewHolder = new ServiceViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ServiceAdapter.ServiceViewHolder holder, int position) {
        final InspireService service = serviceList.get(position);

        holder.serviceNameTextView.setText(service.getServiceName() + " " + service.getServiceTypeName());
        holder.cusNameTextView.setText(service.getCustomerName());
        holder.addressTextView.setText(service.getAddress());
        holder.mobileTextView.setText(service.getMobileNo());
        holder.dateTextView.setText(AppUtils.getNotificationTime(service.getUpdatedDate()));
        holder.customerIdTextView.setText(String.valueOf(service.getCustomerId()));
        if(service.getServiceStatus()!=null) {
            if (service.getServiceStatus().equals(AppConstant.SERVICE_OPEN)) {
                holder.statusTextView.setText(activity.getString(R.string.serviceOpen));
                holder.statusLayout.setCardBackgroundColor(activity.getResources().getColor(R.color.serviceOpenBg));
                holder.statusImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.open_icon));
            } else if (service.getServiceStatus().equals(AppConstant.SERVICE_PROGRESS)) {
                holder.statusTextView.setText(activity.getString(R.string.serviceProgress));
                holder.statusLayout.setCardBackgroundColor(activity.getResources().getColor(R.color.serviceProgressBg));
                holder.statusImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.progress_icon));
            } else if (service.getServiceStatus().equals(AppConstant.SERVICE_COMPLETED)) {
                holder.statusTextView.setText(activity.getString(R.string.serviceCompleted));
                holder.statusLayout.setCardBackgroundColor(activity.getResources().getColor(R.color.serviceCompletedBg));
                holder.statusImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.completed_icon));
            } else if (service.getServiceStatus().equals(AppConstant.SERVICE_REOPEN)) {
                holder.statusTextView.setText(activity.getString(R.string.serviceReopen));
                holder.statusLayout.setCardBackgroundColor(activity.getResources().getColor(R.color.serviceReopenBg));
                holder.statusImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.reopen_icon));
            } else if (service.getServiceStatus().equals(AppConstant.SERVICE_PENDING)) {
                holder.statusTextView.setText(activity.getString(R.string.servicePending));
                holder.statusLayout.setCardBackgroundColor(activity.getResources().getColor(R.color.servicePendingBg));
                holder.statusImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.pending_icon));
            }else if (service.getServiceStatus().equals(AppConstant.SERVICE_WO_RETURN)) {
                holder.statusTextView.setText(service.getServiceStatus());
                holder.statusImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.return_icon));
            }else if (service.getServiceStatus().equals(AppConstant.SERVICE_READY)) {
                holder.statusTextView.setText(service.getServiceStatus());
                holder.statusImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.ready_icon));
            }else if (service.getServiceStatus().equals(AppConstant.SERVICE_APPROVAL_PENDING)) {
                holder.statusTextView.setText(service.getServiceStatus());
                holder.statusImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.approval_pending_icon));
            }
        }

        holder.callLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onCallMember(service);

            }
        });

        holder.serviceLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.serviceDetail(service);
            }
        });

        holder.commentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent commentIntent = new Intent(activity, ServiceCommentActivity.class);
                commentIntent.putExtra(AppConstant.BOOKING_ID_EXTRA, service);
                activity.startActivity(commentIntent);
            }
        });
        
    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }

    public class ServiceViewHolder extends RecyclerView.ViewHolder {

        private TextView cusNameTextView;
        private TextView serviceNameTextView;
        private TextView addressTextView;
        private TextView mobileTextView;
        private TextView statusTextView;
        private TextView dateTextView;
        private TextView customerIdTextView;

        private LinearLayout callLayout;
        private CardView statusLayout;
        private LinearLayout serviceLayout;
        private LinearLayout commentLayout;

        private ImageView statusImageView;

        public ServiceViewHolder(View itemView) {
            super(itemView);
            cusNameTextView = (TextView) itemView.findViewById(R.id.nameTextView);
            serviceNameTextView = (TextView) itemView.findViewById(R.id.serviceTextView);
            addressTextView = (TextView) itemView.findViewById(R.id.addressTextView);
            mobileTextView = (TextView) itemView.findViewById(R.id.mobileTextView);
            statusTextView = (TextView) itemView.findViewById(R.id.statusTextView);
            dateTextView = (TextView) itemView.findViewById(R.id.dateTextView);
            customerIdTextView = (TextView) itemView.findViewById(R.id.customerID);

            callLayout = (LinearLayout) itemView.findViewById(R.id.callLayout);
            statusLayout = (CardView) itemView.findViewById(R.id.statusLayout);
            serviceLayout = (LinearLayout) itemView.findViewById(R.id.serviceLayout);
            commentLayout = (LinearLayout) itemView.findViewById(R.id.commentLayout);

            statusImageView = (ImageView) itemView.findViewById(R.id.statusImageView);
        }
    }

    public interface OnServiceListener {
        void onCallMember(InspireService service);
        void serviceDetail(InspireService service);
    }

}
