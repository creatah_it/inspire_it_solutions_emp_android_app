package com.creatah.inspireitsolutions.employee.app.modal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by creatah1 on 09-06-2017.
 */

public class Comment implements Serializable {

    @SerializedName("comment_id")
    private int id;

    @SerializedName("service_id")
    private String serviceId;

    @SerializedName("employee_name")
    private String employeeName;

    @SerializedName("status")
    private String status;

    @SerializedName("comment")
    private String comment;

    @SerializedName("created_date")
    private String date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
