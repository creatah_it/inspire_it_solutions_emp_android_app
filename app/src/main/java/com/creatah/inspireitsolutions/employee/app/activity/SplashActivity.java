package com.creatah.inspireitsolutions.employee.app.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;

import com.android.framework.utils.Constants;
import com.android.framework.utils.DeviceManager;
import com.android.framework.utils.Utils;
import com.android.network.listener.ServiceResponseListener;
import com.android.theme.activity.BaseActivity;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.manager.SharedPreferenceManager;
import com.creatah.inspireitsolutions.employee.app.modal.AppInfo;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.loopj.android.http.RequestParams;

public class SplashActivity extends BaseActivity {

    private static final int SPLASH_DELAY = 4000;
    private static final int NEXT_ACTIVITY = 0;

    private Context context;
    private boolean isService = false;
    private final int LOCATION_REQUEST_PERMISSION = 456;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        hideToolbar();
        context = this;

        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                startLocationService();
                getAppInfoService();
            } else if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showAlert(context, getString(R.string.locationpermission), getString(R.string.locationpermissiondes), "Try", Try, "No", No);
                } else {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_PERMISSION);
                }
            }
        } else {
            startLocationService();
            getAppInfoService();
        }

    }

    private void getAppInfo() {

        if (DeviceManager.isDeviceOffline(context)) {
            showAlert(context, getResources().getString(R.string.title_of_data_connection), getResources().getString(R.string.title_of_data_message),
                    getResources().getString(R.string.ok), null, null, null);
            return;
        }
        if (!isService) {
            RequestParams requestParams = new RequestParams();
            requestParams.put("version_code", String.valueOf(Utils.getAppVersion(getApplication())));
            requestParams.put("device_id", Utils.getAndroidSecureId(getApplicationContext()));
            requestParams.put("os_type", getString(R.string.android));

            ServiceManager.getAppInfo(SplashActivity.this, requestParams, new ServiceResponseListener<AppInfo>() {
                        @Override
                        public void onSuccess(AppInfo response) {
                            isService = false;
                            if (response != null && !response.isError()) {
                                AppInfo appInfo = response;
                                AppUtils.setAppInfo(appInfo, getApplication());
                                startHomeActivity();
                            }

                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            showAlert(context, getResources().getString(R.string.server_failed), getResources().getString(R.string.server_unavailable), getResources().getString(R.string.ok), null, null, null);

                        }
                    }

            );
        }


    }

    private void startHomeActivity() {
        Message msg = new Message();
        msg.what = NEXT_ACTIVITY;
        splashHandler.sendMessageDelayed(msg, SPLASH_DELAY);
    }

    private final Handler splashHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
                //  Thread.sleep(2000);
                showHomeScreen();
            } catch (Exception ex) {
            }
        }
    };

    private void showHomeScreen() {

        int updateStatus = 0;
        updateStatus = AppUtils.getAppInfo(getApplication()).getUpdateStatus();
        if (updateStatus == Constants.FORCE_UPDATE || updateStatus == Constants.SOFT_UPDATE) {
            startActivity(new Intent(context, AppUpdateActivity.class));
            finish();
        } else {

            if (AppUtils.isAuthenticatedUserInfo(getApplication())) {
                Intent homeActivity = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(homeActivity);
                finish();
            } else {
                Intent homeActivity = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(homeActivity);
                finish();
            }
        }

    }

    private void startLocationService() {
        boolean isLocationService = SharedPreferenceManager.getPreferenceBooleanValue(getApplicationContext(), SharedPreferenceManager.Keys.IS_LOCATION_SERVICE);
        if (!isLocationService) {
            AppUtils.startLocationService(this);
        }
    }

    private void getAppInfoService() {
        if (AppUtils.isAuthenticatedAppInfo(getApplication())) {
            startHomeActivity();
        } else {
            getAppInfo();
        }
    }

    DialogInterface.OnClickListener Try = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_PERMISSION);
        }
    };


    DialogInterface.OnClickListener No = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_PERMISSION:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    startLocationService();

                }

                getAppInfoService();
                break;


        }
    }

}
