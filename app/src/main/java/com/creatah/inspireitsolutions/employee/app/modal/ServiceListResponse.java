package com.creatah.inspireitsolutions.employee.app.modal;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by creatah1 on 03-06-2017.
 */

public class ServiceListResponse extends BaseResponse {

    @SerializedName("booking_details")
    private List<InspireService> serviceList;

    private int total_count;

    private int page_count;

    public List<InspireService> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<InspireService> serviceList) {
        this.serviceList = serviceList;
    }

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public int getPage_count() {
        return page_count;
    }

    public void setPage_count(int page_count) {
        this.page_count = page_count;
    }
}
