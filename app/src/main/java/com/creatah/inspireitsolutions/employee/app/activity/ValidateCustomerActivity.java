package com.creatah.inspireitsolutions.employee.app.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.framework.utils.DeviceManager;
import com.android.framework.utils.Utils;
import com.android.network.listener.ServiceResponseListener;
import com.android.theme.activity.BaseActivity;
import com.android.theme.utils.Constants;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.modal.Customer;
import com.creatah.inspireitsolutions.employee.app.modal.ExistingCustomerResponse;
import com.creatah.inspireitsolutions.employee.app.utils.AppConstant;
import com.loopj.android.http.RequestParams;

/**
 * Created by creatah1 on 07-08-2017.
 */

public class ValidateCustomerActivity extends BaseActivity {

    private EditText mobileEditText;
    private Button submitButton;
    private Context context;
    private boolean isService;
    private int BOOK_SERVICE_REQ_CODE = 746;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_customer);
        setTitle(getString(R.string.validateCustomerTitle));
        initializeViews();
        context = this;
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateData();
            }
        });
    }

    private void validateData() {
        String mobile = mobileEditText.getText().toString();
        if (TextUtils.isEmpty(mobile)) {
            mobileEditText.setError(getString(R.string.enterMobile));
        } else if (mobile.length() != 10) {
            mobileEditText.setError(getString(R.string.invalidMobile));
        } else {
            validateCustomer(mobile);
        }
    }

    private void validateCustomer(final String mobile) {

        if (DeviceManager.isDeviceOffline(context)) {

            showAlert(context, getResources().getString(R.string.title_of_data_connection), getResources().getString(R.string.title_of_data_message),
                    getResources().getString(R.string.ok), null, null, null);
            return;
        }

        RequestParams requestParams = new RequestParams();
        requestParams.put("mobile_no", mobile);

        if (!isService) {
            mainCategoryHandler.sendEmptyMessage(Constants.SHOW_PROGRESS_DIALOG);
            isService = true;
            ServiceManager.checkExistingCustomer(context, requestParams, new ServiceResponseListener<ExistingCustomerResponse>() {
                        @Override
                        public void onSuccess(ExistingCustomerResponse response) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);

                            if (response != null && !response.isError()) {
                                Customer customer = response.getCustomerDetails();
                                Intent bookIntent = new Intent(context, CreateGeneralServiceActivity.class);
                                bookIntent.putExtra(AppConstant.CUSTOMER_DETAILS_EXTRA, customer);
                                bookIntent.putExtra(AppConstant.CUSTOMER_MOBILE_EXTRA, mobile);
                                startActivityForResult(bookIntent, BOOK_SERVICE_REQ_CODE);
                            } else if (response != null && response.isError()) {
                                String message = response.getMessage();
                                Utils.displayToast(context, message, Toast.LENGTH_SHORT);
                            } else {
                                Utils.displayToast(context, getString(R.string.server_unavailable), Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            showAlert(context, getResources().getString(R.string.server_failed), getResources().getString(R.string.server_unavailable),
                                    getResources().getString(R.string.ok), null, null, null);
                        }
                    }

            );
        }


    }

    private void initializeViews() {
        mobileEditText = (EditText) findViewById(R.id.mobileEditText);
        submitButton = (Button) findViewById(R.id.submitButton);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BOOK_SERVICE_REQ_CODE && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }
}
