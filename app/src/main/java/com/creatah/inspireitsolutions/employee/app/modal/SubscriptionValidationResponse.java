package com.creatah.inspireitsolutions.employee.app.modal;

import com.google.gson.annotations.SerializedName;

/**
 * Created by creatah1 on 07-08-2017.
 */

public class SubscriptionValidationResponse extends BaseResponse {

    @SerializedName("subscription_details")
    private Subscription annualSubscription;

    public Subscription getAnnualSubscription() {
        return annualSubscription;
    }

    public void setAnnualSubscription(Subscription annualSubscription) {
        this.annualSubscription = annualSubscription;
    }
}
