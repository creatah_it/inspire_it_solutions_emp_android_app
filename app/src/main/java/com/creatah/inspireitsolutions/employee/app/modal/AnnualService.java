package com.creatah.inspireitsolutions.employee.app.modal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by creatah1 on 16-06-2017.
 */

public class AnnualService implements Serializable {

    @SerializedName("booking_id")
    private int serviceId;

    @SerializedName("customer_name")
    private String customerName;

    @SerializedName("customer_id")
    private int customerId;

    @SerializedName("email_id")
    private String emailId;

    @SerializedName("mobile_no")
    private String mobileNo;

    @SerializedName("address")
    private String address;

    @SerializedName("area_id")
    private String areaId;

    @SerializedName("area_name")
    private String areaName;

    @SerializedName("annual_sevice_id")
    private int annualServiceId;

    @SerializedName("annual_sevice_name")
    private String annualService;

    @SerializedName("annual_sevice_type_id")
    private int annualServiceTypeId;

    @SerializedName("annual_sevice_type_name")
    private String annualServiceType;

    @SerializedName("validity_years")
    private String validityYear;

    @SerializedName("amount")
    private int amount;

    @SerializedName("start_date")
    private String startDate;

    @SerializedName("end_date")
    private String endDate;

    @SerializedName("description")
    private String description;

    @SerializedName("status")
    private String serviceStatus;

    @SerializedName("status_id")
    private int statusId;

    @SerializedName("created_date")
    private String createdDate;

    @SerializedName("updated_date")
    private String updatedDate;

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public int getAnnualServiceId() {
        return annualServiceId;
    }

    public void setAnnualServiceId(int annualServiceId) {
        this.annualServiceId = annualServiceId;
    }

    public String getAnnualService() {
        return annualService;
    }

    public void setAnnualService(String annualService) {
        this.annualService = annualService;
    }

    public int getAnnualServiceTypeId() {
        return annualServiceTypeId;
    }

    public void setAnnualServiceTypeId(int annualServiceTypeId) {
        this.annualServiceTypeId = annualServiceTypeId;
    }

    public String getAnnualServiceType() {
        return annualServiceType;
    }

    public void setAnnualServiceType(String annualServiceType) {
        this.annualServiceType = annualServiceType;
    }

    public String getValidityYear() {
        return validityYear;
    }

    public void setValidityYear(String validityYear) {
        this.validityYear = validityYear;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
