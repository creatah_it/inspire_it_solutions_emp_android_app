package com.creatah.inspireitsolutions.employee.app.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.framework.utils.DeviceManager;
import com.android.framework.utils.Utils;
import com.android.network.listener.ServiceResponseListener;
import com.android.theme.activity.DialogActivity;
import com.android.theme.utils.Constants;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.modal.ForgotPasswordResponse;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.loopj.android.http.RequestParams;

/**
 * Created by creatah1 on 17-06-2017.
 */

public class ForgotPasswordActivity extends DialogActivity {

    private boolean isService;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);
        getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
        context = this;
        final EditText mobilEditText = (EditText) findViewById(R.id.mobileEditText);
        Button submitButton = (Button) findViewById(R.id.submitButton);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mobileNo = mobilEditText.getText().toString();
                if (TextUtils.isEmpty(mobileNo)) {
                    mobilEditText.setError(getString(R.string.userName));
                } else if (AppUtils.isValidEmail(mobileNo) || (mobileNo.matches("[0-9]+") && mobileNo.length() == 10)) {
                    sendMobileNo(mobileNo);
                } else {
                    mobilEditText.setError(getString(R.string.invalidUsername));
                }


            }
        });

    }

    private void sendMobileNo(String mobileNo) {
        if (DeviceManager.isDeviceOffline(context)) {

            Utils.displayToast(context, getString(R.string.server_unavailable), Toast.LENGTH_SHORT);

            return;
        }

        RequestParams requestParams = new RequestParams();
        requestParams.put("user_name", mobileNo);

        if (!isService) {
            mainCategoryHandler.sendEmptyMessage(Constants.SHOW_PROGRESS_DIALOG);
            isService = true;
            ServiceManager.forgotPassword(context, requestParams, new ServiceResponseListener<ForgotPasswordResponse>() {
                        @Override
                        public void onSuccess(ForgotPasswordResponse response) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);

                            if (response != null && !response.isError()) {
                                String message = response.getMessage();
                                Utils.displayToast(context, message, Toast.LENGTH_SHORT);
                                finish();
                            } else if (response != null) {
                                String message = response.getMessage();
                                Utils.displayToast(context, message, Toast.LENGTH_SHORT);
                            } else {
                                Utils.displayToast(context, getString(R.string.server_unavailable), Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            Utils.displayToast(context, getString(R.string.server_unavailable), Toast.LENGTH_SHORT);

                        }
                    }

            );
        }


    }
}
