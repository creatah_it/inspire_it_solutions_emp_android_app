package com.creatah.inspireitsolutions.employee.app.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.framework.utils.DeviceManager;
import com.android.framework.utils.Utils;
import com.android.network.listener.ServiceResponseListener;
import com.android.theme.activity.BaseActivity;
import com.android.theme.utils.Constants;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.modal.LoginResponse;
import com.creatah.inspireitsolutions.employee.app.modal.UserInfo;
import com.creatah.inspireitsolutions.employee.app.utils.AppConstant;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.loopj.android.http.RequestParams;


/**
 * Created by creatah1 on 22-05-2017.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private EditText mobileEditText;
    private EditText passwordEditText;
    private TextView forgotTextView;
    private Button loginButton;

    private Context context;
    private boolean isService = false;
    private final int SMS_REQUEST_PERMISSION = 746;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        hideToolbar();
        initializeViews();
        context = this;
        forgotTextView.setOnClickListener(this);
        loginButton.setOnClickListener(this);

    }

    private void initializeViews() {
        mobileEditText = (EditText) findViewById(R.id.mobileEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        forgotTextView = (TextView) findViewById(R.id.forgotPassword);
        loginButton = (Button) findViewById(R.id.loginButton);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.forgotPassword:
                startActivity(new Intent(context, ForgotPasswordActivity.class));
                break;

            case R.id.loginButton:
                validateData();
                break;


        }
    }


    private void validateData() {

        String userEmail = mobileEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        if (TextUtils.isEmpty(userEmail)) {
            mobileEditText.setError(getString(R.string.enterEmail));
        } else if (!AppUtils.isValidEmail(userEmail)) {
            mobileEditText.setError(getString(R.string.invalidEmail));
        } else if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.enterPassword));
        } else {
            RequestParams requestParams = new RequestParams();
            requestParams.put("email_id", userEmail);
            requestParams.put("password", password);
            doLogin(requestParams);
        }

    }

    private void doLogin(RequestParams requestParams) {

        if (DeviceManager.isDeviceOffline(context)) {

            showAlert(context, getResources().getString(R.string.title_of_data_connection), getResources().getString(R.string.title_of_data_message),
                    getResources().getString(R.string.ok), null, null, null);
            return;
        }

        if (!isService) {
            mainCategoryHandler.sendEmptyMessage(Constants.SHOW_PROGRESS_DIALOG);
            isService = true;
            ServiceManager.doLogin(context, requestParams, new ServiceResponseListener<LoginResponse>() {
                        @Override
                        public void onSuccess(LoginResponse response) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);

                            if (response != null && !response.isError()) {
                                AppUtils.setUserInfo(response.getUserInfo(), getApplication());
                                startActivity(new Intent(context, HomeActivity.class));
                                finish();
                            } else if (response != null && response.isError()) {
                                String message = response.getMessage();
                                Utils.displayToast(context, message, Toast.LENGTH_SHORT);
                            }else {
                                Utils.displayToast(context, getString(R.string.server_unavailable), Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            showAlert(context, getResources().getString(R.string.server_failed), getResources().getString(R.string.server_unavailable),
                                    getResources().getString(R.string.ok), null, null, null);
                        }
                    }

            );
        }

    }


    DialogInterface.OnClickListener Try = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            ActivityCompat.requestPermissions(LoginActivity.this,
                    new String[]{android.Manifest.permission.READ_SMS},
                    SMS_REQUEST_PERMISSION);
        }
    };


    DialogInterface.OnClickListener No = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case SMS_REQUEST_PERMISSION:
                forgotValidation();
                break;


        }
    }

    private void forgotValidation() {
        String userName = mobileEditText.getText().toString();
        if (TextUtils.isEmpty(userName)) {
            mobileEditText.setError(getString(R.string.enterEmail));
        } else if (!AppUtils.isValidEmail(userName)) {
            mobileEditText.setError(getString(R.string.invalidEmail));
        } else {
            requestRePassword(userName);
        }

    }

    private void requestRePassword(final String emailID) {
       /* RequestParams requestParams = new RequestParams();
        requestParams.put("email_id", emailID);

        if (DeviceManager.isDeviceOffline(context)) {

            showAlert(context, getResources().getString(R.string.title_of_data_connection), getResources().getString(R.string.title_of_data_message),
                    getResources().getString(R.string.ok), null, null, null);
            return;
        }

        if (!isService) {
            mainCategoryHandler.sendEmptyMessage(Constants.SHOW_PROGRESS_DIALOG);
            isService = true;
            ServiceManager.forgotPassword(context, requestParams, new ServiceResponseListener<ForgotPasswordResponse>() {
                        @Override
                        public void onSuccess(ForgotPasswordResponse response) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);

                            if (response != null && !response.isError()) {
                                startActivity(new Intent(context, ForgotPasswordOtpActivity.class).putExtra(AppConstant.SIGNUP_EMAIL_EXTRA, emailID));
                            } else if (response != null) {
                                String message = response.getMessage();
                                Utils.displayToast(context, message, Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            showAlert(context, getResources().getString(R.string.server_failed), getResources().getString(R.string.server_unavailable),
                                    getResources().getString(R.string.ok), null, null, null);
                        }
                    }

            );
        }*/

    }


}
