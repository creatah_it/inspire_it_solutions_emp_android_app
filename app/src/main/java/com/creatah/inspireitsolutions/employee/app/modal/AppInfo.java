package com.creatah.inspireitsolutions.employee.app.modal;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by creatah1 on 26-05-2017.
 */

public class AppInfo extends BaseResponse {

    @SerializedName("update_status")
    private int updateStatus;

    @SerializedName("area_list")
    private List<Area> areaList;

    @SerializedName("service_list")
    private List<GeneralService> serviceList;

    @SerializedName("status_list")
    private List<ServiceStatus> statusList;

    public List<ServiceStatus> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<ServiceStatus> statusList) {
        this.statusList = statusList;
    }

    public int getUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(int updateStatus) {
        this.updateStatus = updateStatus;
    }

    public List<Area> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<Area> areaList) {
        this.areaList = areaList;
    }

    public List<GeneralService> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<GeneralService> serviceList) {
        this.serviceList = serviceList;
    }
}
