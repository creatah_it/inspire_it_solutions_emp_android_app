package com.creatah.inspireitsolutions.employee.app.receiver;

/**
 * Created by karthikeyan on 12-05-2016.
 */

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.creatah.inspireitsolutions.employee.app.listener.UiUpdateListener;
import com.creatah.inspireitsolutions.employee.app.sqllite.DBHandler;
import com.creatah.inspireitsolutions.employee.app.sqllite.Locationlist;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.creatah.inspireitsolutions.employee.app.utils.NetworkUtil;
import com.loopj.android.http.RequestParams;

import java.util.List;


public class NetworkChangeReceiver extends BroadcastReceiver {
    DBHandler locationDBHandler;

    List<Locationlist> locationlists;

    private int locationListId;


    @Override
    public void onReceive(final Context context, final Intent intent) {
        Application mApplication = ((Application) context.getApplicationContext());
        boolean status = NetworkUtil.getConnectivityStatusString(context);

        if (status && AppUtils.isAuthenticatedUserInfo(mApplication)) {
            locationDBHandler = new DBHandler(context);
            locationlists = locationDBHandler.getAllLocations();
            if (locationlists.size() > 0) {

                for (final Locationlist shop : locationlists) {
                    RequestParams requestParams = new RequestParams();
                    requestParams.put("emp_id", String.valueOf(AppUtils.getUserInfo(mApplication).getId()));
                    requestParams.put("latitude", shop.getLatitude());
                    requestParams.put("longitude", shop.getLongitude());
                    requestParams.put("date", shop.getDate());
                    requestParams.put("time", shop.getTime());
                    locationListId = shop.getId();
                    AppUtils.updateLocation(context, requestParams, uiUpdateListenerLocationlist);
                    Log.d("network changed", "onReceive");
                }
                locationDBHandler.deleteAllRow();
            }

        }
    }

    private final UiUpdateListener uiUpdateListenerLocationlist = new UiUpdateListener() {

        @Override
        public void OnUpdateUI(Object... object) {
           // locationDBHandler.deleteinsertedRow(locationListId);
        }

        @Override
        public void OnFailure(String errorMessage) {


        }
    };


}
