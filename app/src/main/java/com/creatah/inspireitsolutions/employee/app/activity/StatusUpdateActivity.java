package com.creatah.inspireitsolutions.employee.app.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.framework.utils.DeviceManager;
import com.android.framework.utils.Utils;
import com.android.network.listener.ServiceResponseListener;
import com.android.theme.activity.DialogActivity;
import com.android.theme.utils.Constants;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.modal.AnnualService;
import com.creatah.inspireitsolutions.employee.app.modal.AppInfo;
import com.creatah.inspireitsolutions.employee.app.modal.InspireService;
import com.creatah.inspireitsolutions.employee.app.modal.ServiceStatus;
import com.creatah.inspireitsolutions.employee.app.modal.UpdateServiceResponse;
import com.creatah.inspireitsolutions.employee.app.utils.AppConstant;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by creatah1 on 09-06-2017.
 */

public class StatusUpdateActivity extends DialogActivity {

    private EditText commentEditText;
    private Spinner statusSpinner;
    private Button updateButton;
    private Context context;

    private boolean isService;
    private InspireService service;

    private List<ServiceStatus> serviceStausList = new ArrayList<>();
    private List<String> serviceStatusSTRList = new ArrayList<>();

    private int statusPos = 0;
    private String statusType = null;

    private boolean isGeneralService;
    private AnnualService annualService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_update);
       /* DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);
        getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.MATCH_PARENT);*/
        context = this;
        initializeViews();
        getBundle();

        AppInfo appInfo = AppUtils.getAppInfo(getApplication());
        if (appInfo != null && appInfo.getStatusList() != null && appInfo.getStatusList().size() > 0) {

            serviceStausList = appInfo.getStatusList();
            if (serviceStausList.size() > 0) {
                serviceStatusSTRList.add(getString(R.string.selectStatus));
                for (ServiceStatus status : serviceStausList) {
                    serviceStatusSTRList.add(status.getType());
                }
                ArrayAdapter<String> modaladapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, serviceStatusSTRList);
                statusSpinner.setAdapter(modaladapter);
                statusSpinner.setOnItemSelectedListener(modalListener);
                if(isGeneralService) {
                    if (service.getServiceStatus() != null && !TextUtils.isEmpty(service.getServiceStatus())) {
                        findStatusFromId();
                        statusSpinner.setSelection(statusPos);
                    }
                }else {
                    if (annualService.getServiceStatus() != null && !TextUtils.isEmpty(annualService.getServiceStatus())) {
                        findStatusFromId();
                        statusSpinner.setSelection(statusPos);
                    }
                }
            }

        }

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateData();
            }
        });
    }

    private void getBundle() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null && bundle.containsKey(AppConstant.SERVICE_DETAILS_EXTRA)) {
            service = (InspireService) bundle.getSerializable(AppConstant.SERVICE_DETAILS_EXTRA);
            isGeneralService = true;
        }else  if (bundle != null && bundle.containsKey(AppConstant.ANNUAL_SERVICE_EXTRA)) {
            annualService = (AnnualService) bundle.getSerializable(AppConstant.ANNUAL_SERVICE_EXTRA);
            isGeneralService = false;
        }

    }

    private void validateData() {

        if (statusType == null) {
            Toast.makeText(context, getString(R.string.selectStatus), Toast.LENGTH_SHORT).show();
        } else {

            showAlert(context, getResources().getString(R.string.alert), getResources().getString(R.string.statusUpdateAlert),
                    getResources().getString(R.string.yes), YES, getResources().getString(R.string.no), No);


        }

    }

    DialogInterface.OnClickListener YES = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            makeServiceCall();
        }
    };

    DialogInterface.OnClickListener No = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

        }
    };

    private void findStatusFromId() {

        if(isGeneralService) {
            for (int i = 0; i < serviceStausList.size(); i++) {
                if (service.getServiceStatus().equals(String.valueOf(serviceStausList.get(i).getType()))) {
                    statusPos = i + 1;
                }
            }
        }else {
            for (int i = 0; i < serviceStausList.size(); i++) {
                if (annualService.getServiceStatus().equals(String.valueOf(serviceStausList.get(i).getType()))) {
                    statusPos = i + 1;
                }
            }
        }

    }

    private void initializeViews() {
        commentEditText = (EditText) findViewById(R.id.commentEditText);
        statusSpinner = (Spinner) findViewById(R.id.statusSpinner);
        updateButton = (Button) findViewById(R.id.updateButton);
    }

    private void makeServiceCall() {

        if (DeviceManager.isDeviceOffline(context)) {

            showAlert(context, getResources().getString(R.string.title_of_data_connection), getResources().getString(R.string.title_of_data_message),
                    getResources().getString(R.string.ok), null, null, null);
            return;
        }

        String comment = commentEditText.getText().toString();


        RequestParams requestParams = new RequestParams();
        requestParams.put("employee_id", String.valueOf(AppUtils.getUserInfo(getApplication()).getId()));
        if(isGeneralService) {
            requestParams.put("service_type", AppConstant.SERVICE_TYPE_GENERAL);
            requestParams.put("service_id", String.valueOf(service.getId()));
        }else {
            requestParams.put("service_type", AppConstant.SERVICE_TYPE_ANNUAL);
            requestParams.put("service_id", String.valueOf(annualService.getServiceId()));
        }
        requestParams.put("status_id", statusType);
        requestParams.put("comments", comment);

        if (!isService) {
            mainCategoryHandler.sendEmptyMessage(Constants.SHOW_PROGRESS_DIALOG);
            isService = true;
            ServiceManager.updateServiceStatus(context, requestParams, new ServiceResponseListener<UpdateServiceResponse>() {
                        @Override
                        public void onSuccess(UpdateServiceResponse response) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);

                            if (response != null && !response.isError()) {
                                setResult(RESULT_OK);
                                finish();
                            } else if (response != null) {
                                String message = response.getMessage();
                                Utils.displayToast(context, message, Toast.LENGTH_SHORT);
                            } else {
                                Utils.displayToast(context, getString(R.string.server_unavailable), Toast.LENGTH_SHORT);
                            }

                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            showAlert(context, getResources().getString(R.string.server_failed), getResources().getString(R.string.server_unavailable),
                                    getResources().getString(R.string.ok), null, null, null);
                        }
                    }

            );
        }


    }

    public AdapterView.OnItemSelectedListener modalListener = new AdapterView.OnItemSelectedListener() {


        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position != 0) {
                statusType = String.valueOf(serviceStausList.get(position - 1).getId());
            } else {
                statusType = null;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


}
