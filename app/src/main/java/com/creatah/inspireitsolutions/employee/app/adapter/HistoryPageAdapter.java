package com.creatah.inspireitsolutions.employee.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.creatah.inspireitsolutions.employee.app.fragment.CompletedAnnualServiceFragment;
import com.creatah.inspireitsolutions.employee.app.fragment.CompletedServiceFragment;

/**
 * Created by creatah1 on 16-06-2017.
 */

public class HistoryPageAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public HistoryPageAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {


        switch (position) {

            case 0:
                CompletedServiceFragment tab1 = new CompletedServiceFragment();
                return tab1;

            case 1:
                CompletedAnnualServiceFragment tab2 = new CompletedAnnualServiceFragment();
                return tab2;


            default:
                return new CompletedServiceFragment();

        }


    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}