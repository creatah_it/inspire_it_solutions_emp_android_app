package com.creatah.inspireitsolutions.employee.app.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.framework.utils.DeviceManager;
import com.android.framework.utils.Utils;
import com.android.network.listener.ServiceResponseListener;
import com.android.theme.activity.BaseActivity;
import com.android.theme.utils.Constants;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.modal.AddGeneralServiceResponse;
import com.creatah.inspireitsolutions.employee.app.modal.AppInfo;
import com.creatah.inspireitsolutions.employee.app.modal.Area;
import com.creatah.inspireitsolutions.employee.app.modal.Customer;
import com.creatah.inspireitsolutions.employee.app.modal.GeneralService;
import com.creatah.inspireitsolutions.employee.app.modal.ServiceType;
import com.creatah.inspireitsolutions.employee.app.utils.AppConstant;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by creatah1 on 07-08-2017.
 */

public class CreateGeneralServiceActivity extends BaseActivity implements View.OnClickListener {

    private EditText mobileEditText;
    private EditText nameEditText;
    private EditText emailEditText;
    private EditText addressEditText;
    private EditText brandEditText;
    private EditText modelEditText;
    private EditText serialEditText;
    private EditText descriptionEditText;

    private TextView areaTextView;
    private Spinner areaSpinner;
    private Spinner serviceSpinner;
    private Spinner serviceTypeSpinner;

    private LinearLayout pickLayout;
    private LinearLayout instoreLayout;
    private LinearLayout onsiteLayout;
    private Button addButton;

    private int serviceId = 0;
    private String serviceName;

    private int serviceTypeId = 0;
    private String serviceType;
    private String engagementType = null;

    private List<GeneralService> serviceList = new ArrayList<>();
    private List<String> serviceSTRList = new ArrayList<>();

    private List<ServiceType> serviceTypeList = new ArrayList<>();
    private List<String> serviceTypeSTRList = new ArrayList<>();

    private Context context;
    private boolean isService;

    private Customer customer;

    private LinearLayout areaLayout;

    private List<Area> areaList;
    private List<String> areaString = new ArrayList<>();

    private String areaId = null;

    private String mobileNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generel_service_booking);
        setTitle(getString(R.string.createService));
        context = this;
        initializeViews();
        getBundle();

        AppInfo appInfo = AppUtils.getAppInfo(getApplication());
        if (appInfo != null && appInfo.getServiceList() != null && appInfo.getServiceList().size() > 0) {
            serviceList = appInfo.getServiceList();
            if (serviceList.size() > 0) {
                serviceSTRList.add(getString(R.string.selectService));
                for (GeneralService service : serviceList) {
                    serviceSTRList.add(service.getServiceName());
                }
                ArrayAdapter<String> serviceAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, serviceSTRList);
                serviceSpinner.setAdapter(serviceAdapter);
                serviceSpinner.setOnItemSelectedListener(serviceListener);
            }

        }

        if (appInfo != null && appInfo.getAreaList() != null && appInfo.getAreaList().size() > 0) {
            areaList = appInfo.getAreaList();
            areaString.add(getString(R.string.selectArea));
            for (Area list : areaList) {
                areaString.add(list.getAreaName());
            }
            ArrayAdapter<String> modaladapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, areaString);
            areaSpinner.setAdapter(modaladapter);
            areaSpinner.setOnItemSelectedListener(areaListener);
        }

        if (customer != null) {
            setEditTextEditable(false);
            areaTextView.setVisibility(View.VISIBLE);
            areaLayout.setVisibility(View.GONE);
            mobileEditText.setText(customer.getMobileNo());
            nameEditText.setText(customer.getName());
            emailEditText.setText(customer.getEmailId());
            addressEditText.setText(customer.getAddress());
            areaTextView.setText(customer.getArea());
        } else {
            mobileEditText.setText(mobileNo);
            setEditTextEditable(true);
            areaTextView.setVisibility(View.GONE);
            areaLayout.setVisibility(View.VISIBLE);
        }

        addButton.setOnClickListener(this);
        pickLayout.setOnClickListener(this);
        instoreLayout.setOnClickListener(this);
        onsiteLayout.setOnClickListener(this);
    }

    private void setEditTextEditable(boolean status) {
        mobileEditText.setFocusable(false);
        mobileEditText.setFocusableInTouchMode(false);
        mobileEditText.setClickable(false);

        nameEditText.setFocusable(status);
        nameEditText.setFocusableInTouchMode(status);
        nameEditText.setClickable(status);

        emailEditText.setFocusable(status);
        emailEditText.setFocusableInTouchMode(status);
        emailEditText.setClickable(status);

        addressEditText.setFocusable(status);
        addressEditText.setFocusableInTouchMode(status);
        addressEditText.setClickable(status);
    }

    private void getBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(AppConstant.CUSTOMER_DETAILS_EXTRA)) {
            customer = (Customer) bundle.getSerializable(AppConstant.CUSTOMER_DETAILS_EXTRA);
            mobileNo = bundle.getString(AppConstant.CUSTOMER_MOBILE_EXTRA);
        }
    }

    private void initializeViews() {

        mobileEditText = (EditText) findViewById(R.id.mobileEditText);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        nameEditText = (EditText) findViewById(R.id.nameEditText);
        addressEditText = (EditText) findViewById(R.id.addressEditText);
        brandEditText = (EditText) findViewById(R.id.brandEditText);
        modelEditText = (EditText) findViewById(R.id.modelEditText);
        serialEditText = (EditText) findViewById(R.id.serialNoEditText);
        descriptionEditText = (EditText) findViewById(R.id.descriptionEditText);

        areaTextView = (TextView) findViewById(R.id.areaTextView);

        areaSpinner = (Spinner) findViewById(R.id.areaSpinner);
        serviceSpinner = (Spinner) findViewById(R.id.serviceSpinner);
        serviceTypeSpinner = (Spinner) findViewById(R.id.serviceTypeSpinner);
        pickLayout = (LinearLayout) findViewById(R.id.pickUp);
        instoreLayout = (LinearLayout) findViewById(R.id.instore);
        onsiteLayout = (LinearLayout) findViewById(R.id.onsite);

        addButton = (Button) findViewById(R.id.createButton);
        areaLayout = (LinearLayout) findViewById(R.id.areaLayout);
    }


    public AdapterView.OnItemSelectedListener serviceListener = new AdapterView.OnItemSelectedListener() {


        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position != 0) {
                serviceId = (serviceList.get(position - 1).getId());
                serviceName = (serviceList.get(position - 1).getServiceName());

                serviceTypeList = serviceList.get(position - 1).getServiceTypeList();
                if (serviceTypeList.size() > 0) {
                    serviceTypeSTRList.clear();
                    serviceTypeSTRList.add(getString(R.string.selectServiceType));
                    for (ServiceType serviceType : serviceTypeList) {
                        serviceTypeSTRList.add(serviceType.getServiceType());
                    }
                    ArrayAdapter<String> modaladapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, serviceTypeSTRList);
                    serviceTypeSpinner.setAdapter(modaladapter);
                    serviceTypeSpinner.setOnItemSelectedListener(modalListener);
                }

            } else {
                serviceId = 0;
                serviceName = null;
                serviceTypeSTRList.clear();
                serviceTypeSTRList.add(getString(R.string.selectServiceType));
                ArrayAdapter<String> modaladapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, serviceTypeSTRList);
                serviceTypeSpinner.setAdapter(modaladapter);
                serviceTypeSpinner.setOnItemSelectedListener(modalListener);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    public AdapterView.OnItemSelectedListener modalListener = new AdapterView.OnItemSelectedListener() {


        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position != 0) {
                serviceTypeId = (serviceTypeList.get(position - 1).getServiceTypeId());
                serviceType = (serviceTypeList.get(position - 1).getServiceType());

            } else {
                serviceTypeId = 0;
                serviceType = null;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    public AdapterView.OnItemSelectedListener areaListener = new AdapterView.OnItemSelectedListener() {


        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position != 0) {
                areaId = String.valueOf(areaList.get(position - 1).getId());
            } else {
                areaId = null;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.createButton:
                validateData();
                break;
            case R.id.pickUp:
                engagementType = getString(R.string.pickUpDrop);
                if (android.os.Build.VERSION.SDK_INT > 15) {
                    instoreLayout.setBackground(getResources().getDrawable(R.drawable.edittextsingupbg));
                    onsiteLayout.setBackground(getResources().getDrawable(R.drawable.edittextsingupbg));
                    pickLayout.setBackground(getResources().getDrawable(R.drawable.buttonprimarybgselect));
                }

                break;

            case R.id.instore:
                engagementType = getString(R.string.instoreAppt);
                if (android.os.Build.VERSION.SDK_INT > 15) {
                    onsiteLayout.setBackground(getResources().getDrawable(R.drawable.edittextsingupbg));
                    pickLayout.setBackground(getResources().getDrawable(R.drawable.edittextsingupbg));
                    instoreLayout.setBackground(getResources().getDrawable(R.drawable.buttonprimarybgselect));
                }

                break;

            case R.id.onsite:
                engagementType = getString(R.string.onsiteDoor);
                if (android.os.Build.VERSION.SDK_INT > 15) {
                    instoreLayout.setBackground(getResources().getDrawable(R.drawable.edittextsingupbg));
                    pickLayout.setBackground(getResources().getDrawable(R.drawable.edittextsingupbg));
                    onsiteLayout.setBackground(getResources().getDrawable(R.drawable.buttonprimarybgselect));
                }
                break;

        }
    }

    private void validateData() {
        String name = nameEditText.getText().toString();
        String email = emailEditText.getText().toString();
        String mobile = mobileEditText.getText().toString();
        String address = addressEditText.getText().toString();
        String brand = brandEditText.getText().toString();
        String model = modelEditText.getText().toString();
        String serialNo = serialEditText.getText().toString();
        String description = descriptionEditText.getText().toString();
        if (customer == null && TextUtils.isEmpty(name)) {
            nameEditText.setError(getString(R.string.customerName1));
        } else if (customer == null && TextUtils.isEmpty(email)) {
            emailEditText.setError(getString(R.string.customerEmail));
        } else if (customer == null && TextUtils.isEmpty(email)) {
            emailEditText.setError(getString(R.string.customerEmail));
        } else if (customer == null && !AppUtils.isValidEmail(email)) {
            emailEditText.setError(getString(R.string.invalidEmail));
        } else if (customer == null && TextUtils.isEmpty(mobile)) {
            mobileEditText.setError(getString(R.string.customerMobile));
        } else if (customer == null && mobile.length() != 10) {
            mobileEditText.setError(getString(R.string.invalidMobile));
        } else if (customer == null && TextUtils.isEmpty(address)) {
            addressEditText.setError(getString(R.string.customerAddress));
        } else if (customer == null && areaId!=null && areaId.equals("0")) {
            Toast.makeText(context, getString(R.string.selectArea), Toast.LENGTH_SHORT).show();
        } else if (serviceId == 0) {
            Toast.makeText(context, getString(R.string.selectService), Toast.LENGTH_SHORT).show();
        } else if (serviceTypeId == 0) {
            Toast.makeText(context, getString(R.string.selectServiceType), Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(brand)) {
            addressEditText.setError(getString(R.string.enterBrand));
        } else if (TextUtils.isEmpty(model)) {
            addressEditText.setError(getString(R.string.enterModel));
        } else if (TextUtils.isEmpty(serialNo)) {
            addressEditText.setError(getString(R.string.serialNo));
        } else if (TextUtils.isEmpty(description)) {
            addressEditText.setError(getString(R.string.description));
        } else if (engagementType == null) {
            Toast.makeText(context, getString(R.string.selectEngagementType), Toast.LENGTH_SHORT).show();
        } else {
            RequestParams requestParams = new RequestParams();
            requestParams.put("employee_id", String.valueOf(AppUtils.getUserInfo(getApplication()).getId()));
            requestParams.put("branch_id", String.valueOf(AppUtils.getUserInfo(getApplication()).getBranchId()));
            if (customer == null) {
                requestParams.put("customer_name", name);
                requestParams.put("email_id", email);
                requestParams.put("address", address);
                requestParams.put("area_id", areaId);
                requestParams.put("mobile_no", mobile);
            } else {
                requestParams.put("customer_id", String.valueOf(customer.getCustomerId()));
            }
            requestParams.put("service_id", String.valueOf(serviceId));
            requestParams.put("service_type_id", String.valueOf(serviceTypeId));
            requestParams.put("brand", address);
            requestParams.put("model", model);
            requestParams.put("serial_no", serialNo);
            requestParams.put("description", description);
            requestParams.put("engagement_type", engagementType);
            addGeneralService(requestParams);
        }
    }

    private void addGeneralService(RequestParams requestParams) {
        if (DeviceManager.isDeviceOffline(context)) {

            showAlert(context, getResources().getString(R.string.title_of_data_connection), getResources().getString(R.string.title_of_data_message),
                    getResources().getString(R.string.ok), null, null, null);
            return;
        }

        if (!isService) {
            mainCategoryHandler.sendEmptyMessage(Constants.SHOW_PROGRESS_DIALOG);
            isService = true;
            ServiceManager.addGeneralService(context, requestParams, new ServiceResponseListener<AddGeneralServiceResponse>() {
                        @Override
                        public void onSuccess(AddGeneralServiceResponse response) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);

                            if (response != null && !response.isError()) {
                                setResult(RESULT_OK);
                                finish();
                            } else if (response != null && response.isError()) {
                                String message = response.getMessage();
                                Utils.displayToast(context, message, Toast.LENGTH_SHORT);
                            } else {
                                Utils.displayToast(context, getString(R.string.server_unavailable), Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            showAlert(context, getResources().getString(R.string.server_failed), getResources().getString(R.string.server_unavailable),
                                    getResources().getString(R.string.ok), null, null, null);
                        }
                    }

            );
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

}
