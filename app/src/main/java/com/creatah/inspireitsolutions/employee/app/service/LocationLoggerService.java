package com.creatah.inspireitsolutions.employee.app.service;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.framework.utils.DeviceManager;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.sqllite.DBHandler;
import com.creatah.inspireitsolutions.employee.app.sqllite.Locationlist;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.loopj.android.http.RequestParams;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


public class LocationLoggerService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    // Connection detector
    //	ConnectionDetector cd;
    Boolean internet_status = true;
    Double current_lat, current_lng;
    int gps_status = 0;
    private static final String TAG_SUCCESS = "success";
    public static final String DATABASE_NAME = "GPSLOGGERDB";
    public static final String POINTS_TABLE_NAME = "LOCATION_POINTS";
    public static final String TRIPS_TABLE_NAME = "TRIPS";
    private final DecimalFormat sevenSigDigits = new DecimalFormat("0.#######");
    private final DateFormat timestampFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    private static long minTimeMillis = 1000 * 6;
    private static long minDistanceMeters = 10;
    private static float minAccuracyMeters = 35;
    private int lastStatus = 0;
    private static boolean showingDebugToast = false;
    private static final String tag = "GPSLoggerService";

    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;

    /**
     * Called when the activity is first created.
     */
    @SuppressLint("NewApi")
    private void startLoggerService() {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        // ---use the LocationManager class to obtain GPS locations---


    }

    public void internetStatus() {
          /* cd = new ConnectionDetector(getApplicationContext());
           if (!cd.isConnectingToInternet()) {
   			// Internet Connection is not present
   			// stop executing code by return
       		internet_status=false;
   			return;
   		}
       	else
       	{
       		internet_status=true;
       	}*/
    }

    private void shutdownLoggerService() {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        Log.d("service: : ", "connected");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("Asdf", "Connection failed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());
    }


    @Override
    public void onCreate() {
        super.onCreate();
        if (!AppUtils.checkPlayServices(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), getString(R.string.googlePlayServiceNotAvailable), Toast.LENGTH_SHORT).show();
        }
        createLocationRequest();
        buildGoogleApiClient();
        mGoogleApiClient.connect();
        startLoggerService();
        Log.d("service: : ", "started");

        // Display a notification about us starting. We put an icon in the
        // status bar.
        // showNotification();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
        shutdownLoggerService();
        stopLocationUpdates();

    }

    // This is the object that receives interactions from clients. See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public static void setMinTimeMillis(long _minTimeMillis) {
        minTimeMillis = _minTimeMillis;
    }

    public static long getMinTimeMillis() {
        return minTimeMillis;
    }

    public static void setMinDistanceMeters(long _minDistanceMeters) {
        minDistanceMeters = _minDistanceMeters;
    }

    public static long getMinDistanceMeters() {
        return minDistanceMeters;
    }

    public static float getMinAccuracyMeters() {
        return minAccuracyMeters;
    }

    public static void setMinAccuracyMeters(float minAccuracyMeters) {
        LocationLoggerService.minAccuracyMeters = minAccuracyMeters;
    }

    public static void setShowingDebugToast(boolean showingDebugToast) {
        LocationLoggerService.showingDebugToast = showingDebugToast;
    }

    public static boolean isShowingDebugToast() {
        return showingDebugToast;
    }

    /**
     * Class for clients to access. Because we know this service always runs in
     * the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        LocationLoggerService getService() {
            return LocationLoggerService.this;
        }
    }

    //gps location update
    public void locationUpdate(Location location) {
        if (AppUtils.isAuthenticatedUserInfo(getApplication())) {
            Calendar c = Calendar.getInstance();

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String currentDate = df.format(c.getTime());

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String currentTime = sdf.format(c.getTime());

            RequestParams requestParams = new RequestParams();
            requestParams.put("emp_id", String.valueOf(AppUtils.getUserInfo(getApplication()).getId()));
            requestParams.put("latitude", String.valueOf(location.getLatitude()));
            requestParams.put("longitude", String.valueOf(location.getLongitude()));
            requestParams.put("date", currentDate);
            requestParams.put("time", currentTime);


            if (DeviceManager.isDeviceOffline(getApplicationContext())) {

                DBHandler db = new DBHandler(this);
                db.addLocation(new Locationlist(String.valueOf(location.getLatitude()),
                        String.valueOf(location.getLongitude()),
                        currentDate,
                        currentTime));
                List<Locationlist> locations = db.getAllLocations();
                for (Locationlist locationlist : locations) {
                    String log = "Id: " + locationlist.getId() +
                            " ,Latitude: " + locationlist.getLatitude() +
                            " ,Longitude: " + locationlist.getLongitude() +
                            " ,Date: " + locationlist.getDate() +
                            " ,Time: " + locationlist.getTime();
                    Log.d("Locationlist: : ", log);
                }
                return;
            } else {
                AppUtils.updateLocation(getApplicationContext(), requestParams, null);
            }

        }

    }

    public void locationFetch(boolean internet_status, Location loc) {
        if (internet_status) {
            if (AppUtils.isAuthenticatedUserInfo(getApplication())) {
                locationUpdate(loc);
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, listener);
        Log.d("Asdf", "Location update started ..............: ");
    }

    LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                current_lat = location.getLatitude();
                current_lng = location.getLongitude();
                Log.i(tag, "Database opened ok" + location);
                internetStatus();
                locationFetch(internet_status, location);
            } else {
                Log.i(tag, "Database opened ok" + "null");
            }
        }
    };

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, listener);
        Log.d("asdf", "Location update stopped .......................");
    }


}