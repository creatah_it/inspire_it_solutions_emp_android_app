package com.creatah.inspireitsolutions.employee.app.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.modal.Comment;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;

import java.util.List;

/**
 * Created by karthikeyan on 22-08-2016.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {

    private Activity activity;

    private List<Comment> commentList;

    public CommentAdapter(Activity activity, List<Comment> commentList){
        this.activity = activity;
        this.commentList = commentList;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_layout_notification,parent,false);
        CommentViewHolder viewHolder = new CommentViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {

        Comment comment = commentList.get(position);
        holder.dateTextView.setText(AppUtils.getNotificationTime(comment.getDate()));
        holder.messageTextView.setText(comment.getComment() + "\n -" + comment.getEmployeeName());
    }


    @Override
    public int getItemCount() {
        return commentList.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {

        private TextView dateTextView;
        private TextView messageTextView;

        public CommentViewHolder(View itemView) {
            super(itemView);

            dateTextView = (TextView) itemView.findViewById(R.id.dateTextView);
            messageTextView = (TextView) itemView.findViewById(R.id.messageTextView);
        }
    }

}
