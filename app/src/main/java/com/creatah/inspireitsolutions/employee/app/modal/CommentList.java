package com.creatah.inspireitsolutions.employee.app.modal;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by creatah1 on 09-06-2017.
 */

public class CommentList extends BaseResponse {

    @SerializedName("status_comment_list")
    private List<Comment> commentList;

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }
}
