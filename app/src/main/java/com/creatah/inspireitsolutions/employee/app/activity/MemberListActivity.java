package com.creatah.inspireitsolutions.employee.app.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.framework.utils.DeviceManager;
import com.android.network.listener.ServiceResponseListener;
import com.android.theme.activity.BaseActivity;
import com.android.theme.utils.Constants;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.adapter.MemberAdapter;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.modal.Member;
import com.creatah.inspireitsolutions.employee.app.modal.MemberListResponse;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by creatah1 on 03-06-2017.
 */

public class MemberListActivity extends BaseActivity implements MemberAdapter.OnMemberListener {

    private RecyclerView memberRecyclerView;
    private Context context;
    private boolean isService = false;
    private MemberAdapter memberAdapter;
    private List<Member> memberList = new ArrayList<>();
    private TextView noResultFound;
    private Member member;
    private final int CALL_REQUEST_PERMISSION = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_list);
        setTitle(getString(R.string.memberList));
        initializeViews();
        context = this;
        memberRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        getMemberList();

    }


    private void getMemberList() {

        if (DeviceManager.isDeviceOffline(context)) {
            showAlert(context, getResources().getString(R.string.title_of_data_connection), getResources().getString(R.string.title_of_data_message),
                    getResources().getString(R.string.ok), null, null, null);
            memberRecyclerView.setVisibility(View.GONE);
            noResultFound.setVisibility(View.VISIBLE);
            noResultFound.setText(getString(R.string.title_of_data_message));
            return;
        }


        if (!isService) {

            mainCategoryHandler.sendEmptyMessage(Constants.SHOW_PROGRESS_DIALOG);


            RequestParams requestParams = new RequestParams();
            requestParams.put("branch_id", String.valueOf(AppUtils.getUserInfo(getApplication()).getBranchId()));


            ServiceManager.getMemberList(getApplicationContext(), requestParams, new ServiceResponseListener<MemberListResponse>() {
                        @Override
                        public void onSuccess(MemberListResponse response) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            if (response != null && !response.isError() && response.getMemberList() != null && response.getMemberList().size() > 0) {
                                noResultFound.setVisibility(View.GONE);
                                memberRecyclerView.setVisibility(View.VISIBLE);
                                memberList = response.getMemberList();
                                memberAdapter = new MemberAdapter(MemberListActivity.this, memberList, MemberListActivity.this);
                                memberRecyclerView.setAdapter(memberAdapter);
                            } else {
                                memberRecyclerView.setVisibility(View.GONE);
                                noResultFound.setVisibility(View.VISIBLE);
                                noResultFound.setText("No Member Found");
                            }

                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            memberRecyclerView.setVisibility(View.GONE);
                            noResultFound.setVisibility(View.VISIBLE);
                            noResultFound.setText("Server unavailable");
                        }
                    }

            );
        }

    }


    private void initializeViews() {
        memberRecyclerView = (RecyclerView) findViewById(R.id.memberRecyclerView);
        noResultFound = (TextView) findViewById(R.id.noResultFound);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCallMember(Member member) {
        this.member = member;
        onMakeCall();
    }

    public void onMakeCall() {

        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                AppUtils.makeCall(MemberListActivity.this, member.getMobile());
            } else if (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                    showAlert(context, getString(R.string.callpermission), getString(R.string.callpermissiondesc), "Try", Try, "No", No);
                } else {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CALL_REQUEST_PERMISSION);
                }
            }
        } else {
            AppUtils.makeCall(MemberListActivity.this, member.getMobile());
        }

    }

    DialogInterface.OnClickListener Try = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CALL_REQUEST_PERMISSION);
        }
    };


    DialogInterface.OnClickListener No = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CALL_REQUEST_PERMISSION:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    AppUtils.makeCall(MemberListActivity.this, member.getMobile());


                }
                break;


        }
    }
}
