package com.creatah.inspireitsolutions.employee.app.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.framework.utils.DeviceManager;
import com.android.network.listener.ServiceResponseListener;
import com.android.theme.fragment.BaseFragment;
import com.android.theme.utils.Constants;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.activity.ServiceDetailsActivity;
import com.creatah.inspireitsolutions.employee.app.activity.ValidateCustomerActivity;
import com.creatah.inspireitsolutions.employee.app.adapter.ServiceAdapter;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.modal.AppInfo;
import com.creatah.inspireitsolutions.employee.app.modal.InspireService;
import com.creatah.inspireitsolutions.employee.app.modal.ServiceListResponse;
import com.creatah.inspireitsolutions.employee.app.modal.ServiceStatus;
import com.creatah.inspireitsolutions.employee.app.utils.AppConstant;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by creatah1 on 16-06-2017.
 */

public class ServiceFragment extends BaseFragment implements ServiceAdapter.OnServiceListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView serviceRecyclerView;
    private TextView noResultFound;
    private SwipeRefreshLayout swipeRefreshLayout;

    private boolean isService = false;

    private ServiceAdapter serviceAdapter;
    private List<InspireService> serviceList = new ArrayList<>();

    private InspireService service;
    private final int CALL_REQUEST_PERMISSION = 7;
    public static int DETAIL_REQ_CODE = 456;
    private int ADD_REQ_CODE = 476;

    private FloatingActionButton addServiceFab;

    private Spinner statusSpinner;
    private Button filterButton;

    private String statusType = null;

    private List<ServiceStatus> serviceStausList = new ArrayList<>();
    private List<String> serviceStatusSTRList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service_booking, container, false);
        noResultFound = (TextView) view.findViewById(R.id.noResultFound);
        serviceRecyclerView = (RecyclerView) view.findViewById(R.id.serviceRecyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        addServiceFab = (FloatingActionButton) view.findViewById(R.id.addService);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        statusSpinner = (Spinner) view.findViewById(R.id.statusSpinner);
        filterButton = (Button) view.findViewById(R.id.filterButton);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AppInfo appInfo = AppUtils.getAppInfo(getActivity().getApplication());
        if (appInfo != null && appInfo.getStatusList() != null && appInfo.getStatusList().size() > 0) {

            List<ServiceStatus> serviceStausList1 = appInfo.getStatusList();
            if (serviceStausList1.size() > 0) {
                for (ServiceStatus status : serviceStausList1) {
                    if(!status.getType().equals(getString(R.string.completedStatus))) {
                        serviceStausList.add(status);
                    }
                }
            }

            if (serviceStausList.size() > 0) {
                serviceStatusSTRList.add(getString(R.string.statusAll));
                for (ServiceStatus status : serviceStausList) {
                    serviceStatusSTRList.add(status.getType());
                }
                ArrayAdapter<String> modaladapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, serviceStatusSTRList);
                statusSpinner.setAdapter(modaladapter);
                statusSpinner.setOnItemSelectedListener(modalListener);
            }

        }
        serviceRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(false);
                                        getServiceList();
                                    }
                                }
        );

        addServiceFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addIntent = new Intent(getContext(), ValidateCustomerActivity.class);
                startActivityForResult(addIntent, ADD_REQ_CODE);
            }
        });
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getServiceList();
            }
        });
    }

    private void getServiceList() {

        if (DeviceManager.isDeviceOffline(getContext())) {
            showAlert(getContext(), getResources().getString(R.string.title_of_data_connection), getResources().getString(R.string.title_of_data_message),
                    getResources().getString(R.string.ok), null, null, null);
            serviceRecyclerView.setVisibility(View.GONE);
            noResultFound.setVisibility(View.VISIBLE);
            noResultFound.setText(getString(R.string.title_of_data_message));
            return;
        }


        if (!isService) {

            isService = true;

            if (!swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
                mainCategoryHandler.sendEmptyMessage(Constants.SHOW_PROGRESS_DIALOG);
            } else {
                swipeRefreshLayout.setRefreshing(true);
            }


            RequestParams requestParams = new RequestParams();
            requestParams.put("emp_id", String.valueOf(AppUtils.getUserInfo(getActivity().getApplication()).getId()));
            requestParams.put("status_id", statusType);

            ServiceManager.getServiceList(getContext(), requestParams, new ServiceResponseListener<ServiceListResponse>() {
                        @Override
                        public void onSuccess(ServiceListResponse response) {
                            isService = false;

                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            swipeRefreshLayout.setRefreshing(false);
                            if (response != null && !response.isError() && response.getServiceList() != null && response.getServiceList().size() > 0) {

                                noResultFound.setVisibility(View.GONE);
                                serviceRecyclerView.setVisibility(View.VISIBLE);
                                serviceList.clear();
                                serviceList = response.getServiceList();
                                serviceAdapter = new ServiceAdapter(getActivity(), serviceList, ServiceFragment.this);
                                serviceRecyclerView.setAdapter(serviceAdapter);
                            } else {
                                serviceRecyclerView.setVisibility(View.GONE);
                                noResultFound.setVisibility(View.VISIBLE);
                                noResultFound.setText("No Service Found");
                            }

                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            swipeRefreshLayout.setRefreshing(false);
                            serviceRecyclerView.setVisibility(View.GONE);
                            noResultFound.setVisibility(View.VISIBLE);
                            noResultFound.setText("Server unavailable");
                        }
                    }

            );
        }

    }

    @Override
    public void onCallMember(InspireService service) {
        this.service = service;
        onMakeCall();
    }

    @Override
    public void serviceDetail(InspireService service) {
        Intent detailIntent = new Intent(getContext(), ServiceDetailsActivity.class);
        detailIntent.putExtra(AppConstant.SERVICE_DETAILS_EXTRA, service);
        startActivityForResult(detailIntent, DETAIL_REQ_CODE);
    }

    public void onMakeCall() {

        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                AppUtils.makeCall(getActivity(), service.getMobileNo());
            } else if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                    showAlert(getContext(), getString(R.string.callpermission), getString(R.string.callpermissiondesc), "Try", Try, "No", No);
                } else {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CALL_REQUEST_PERMISSION);
                }
            }
        } else {
            AppUtils.makeCall(getActivity(), service.getMobileNo());
        }

    }

    DialogInterface.OnClickListener Try = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CALL_REQUEST_PERMISSION);
        }
    };


    DialogInterface.OnClickListener No = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CALL_REQUEST_PERMISSION:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    AppUtils.makeCall(getActivity(), service.getMobileNo());


                }
                break;


        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DETAIL_REQ_CODE && resultCode == Activity.RESULT_OK) {
            getServiceList();
        }

        if (requestCode == ADD_REQ_CODE && resultCode == Activity.RESULT_OK) {
            getServiceList();
        }
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        getServiceList();
    }

    public AdapterView.OnItemSelectedListener modalListener = new AdapterView.OnItemSelectedListener() {


        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position != 0) {
                statusType = String.valueOf(serviceStausList.get(position - 1).getId());
            } else {
                statusType = null;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

}
