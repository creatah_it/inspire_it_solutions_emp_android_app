package com.creatah.inspireitsolutions.employee.app.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.framework.utils.DeviceManager;
import com.android.framework.utils.Utils;
import com.android.network.listener.ServiceResponseListener;
import com.android.theme.activity.BaseActivity;
import com.android.theme.utils.Constants;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.modal.AddAMCServiceResponse;
import com.creatah.inspireitsolutions.employee.app.modal.AddGeneralServiceResponse;
import com.creatah.inspireitsolutions.employee.app.modal.Subscription;
import com.creatah.inspireitsolutions.employee.app.utils.AppConstant;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.loopj.android.http.RequestParams;

/**
 * Created by creatah1 on 08-08-2017.
 */

public class CreateAMCServiceActivity extends BaseActivity implements View.OnClickListener {

    private TextView mobileTextView;
    private TextView nameTextView;
    private TextView emailTextView;
    private TextView addressTextView;
    private TextView branchTextView;
    private TextView serviceTextView;
    private TextView serviceTypeTextView;
    private EditText descriptionEditText;

    private Button addButton;

    private Context context;
    private boolean isService;

    private Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amc_service_booking);
        setTitle(getString(R.string.createAMC));
        context = this;
        initializeViews();
        getBundle();

        if(subscription!=null){
            nameTextView.setText(subscription.getCustomerName());
            mobileTextView.setText(subscription.getMobileNo());
            emailTextView.setText(subscription.getEmailId());
            addressTextView.setText(subscription.getAddress());
            branchTextView.setText(subscription.getBranchName());
            serviceTextView.setText(subscription.getServiceName());
            serviceTypeTextView.setText(subscription.getServiceTypeName());
        }

        addButton.setOnClickListener(this);

    }

    private void getBundle() {
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null && bundle.containsKey(AppConstant.SUBSCRIPTION_DETAILS_EXTRA)){
            subscription = (Subscription) bundle.getSerializable(AppConstant.SUBSCRIPTION_DETAILS_EXTRA);
        }else {
            finish();
        }
    }

    private void initializeViews() {
        mobileTextView = (TextView) findViewById(R.id.mobileTextView);
        nameTextView = (TextView) findViewById(R.id.nameTextView);
        emailTextView = (TextView) findViewById(R.id.emailTextView);
        addressTextView = (TextView) findViewById(R.id.addressTextView);
        branchTextView = (TextView) findViewById(R.id.branchTextView);
        serviceTextView = (TextView) findViewById(R.id.serviceTextView);
        serviceTypeTextView = (TextView) findViewById(R.id.serviceTypeTextView);

        descriptionEditText = (EditText) findViewById(R.id.descriptionEditText);
        addButton = (Button) findViewById(R.id.createButton);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.createButton:
                String desc = descriptionEditText.getText().toString();
                addAMCService(desc);
                break;

        }
    }

    private void addAMCService(String desc) {

        if (DeviceManager.isDeviceOffline(context)) {

            showAlert(context, getResources().getString(R.string.title_of_data_connection), getResources().getString(R.string.title_of_data_message),
                    getResources().getString(R.string.ok), null, null, null);
            return;
        }

        RequestParams requestParams = new RequestParams();
        requestParams.put("employee_id", String.valueOf(AppUtils.getUserInfo(getApplication()).getId()));
        requestParams.put("subscription_id", String.valueOf(subscription.getSubscriptionId()));
        requestParams.put("description", desc);
        if (!isService) {
            mainCategoryHandler.sendEmptyMessage(Constants.SHOW_PROGRESS_DIALOG);
            isService = true;
            ServiceManager.addAMCService(context, requestParams, new ServiceResponseListener<AddAMCServiceResponse>() {
                        @Override
                        public void onSuccess(AddAMCServiceResponse response) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);

                            if (response != null && !response.isError()) {
                                setResult(RESULT_OK);
                                finish();
                            } else if (response != null && response.isError()) {
                                String message = response.getMessage();
                                Utils.displayToast(context, message, Toast.LENGTH_SHORT);
                            } else {
                                Utils.displayToast(context, getString(R.string.server_unavailable), Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            showAlert(context, getResources().getString(R.string.server_failed), getResources().getString(R.string.server_unavailable),
                                    getResources().getString(R.string.ok), null, null, null);
                        }
                    }

            );
        }

    }

}
