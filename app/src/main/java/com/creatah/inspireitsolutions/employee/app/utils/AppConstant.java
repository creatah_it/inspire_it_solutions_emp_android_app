package com.creatah.inspireitsolutions.employee.app.utils;

/**
 * Created by karthikeyan on 28-07-2016.
 */
public class AppConstant {

    public static String SERVICE_SUPPORT_EXTRA = "ServiceSupportExtra";

    public static String ABOUT_CATEGORY_EXTRA = "AboutCategoryExtra";

    public static String ABOUT_EXTRA = "AboutExtra";

    public static Integer MORNING_ALARM_REQ_CODE = 444;

    public static final String GENERAL_USER = "General";

    public static final String FACKBOOK_USER = "Facebook";

    public static final String GOOGLE_USER = "Google";

    public static final String SERVICE_TYPE_EXTRA = "Service_type_extra";

    public static final String SIGNUP_EMAIL_EXTRA = "signup_email_extra";

    public static final String SIGNUP_TYPE_EXTRA = "singup_type_extra";

    public static final String SIGNUP_PASSWORD_EXTRA = "singup_Password_extra";

    public static final int OTP_ERROR = 500;

    public static final int NEW_USER_ERROR = 501;

    public static final int INVALID_USER_ERROR = 502;

    public static final String PRODUCT_DETAIL_EXTRA = "ProductDetailExtra";

    public static final String ADD_NEW_SERVICE = "AddNewService";

    public static final String SERVICE_OPEN = "Open";

    public static final String SERVICE_PROGRESS = "Progress";

    public static final String SERVICE_COMPLETED = "Completed";

    public static final String SERVICE_REOPEN = "Reopen";

    public static final String SERVICE_PENDING = "Pending";

    public static final String SERVICE_WO_RETURN = "W/S Return";

    public static final String SERVICE_READY= "Ready";

    public static final String SERVICE_APPROVAL_PENDING = "Approval Pending";

    public static final String SERVICE_DETAILS_EXTRA = "ServiceDetailsExtra";

    public static final String BOOKING_ID_EXTRA = "BookingIdExtra";

    public static final String ANNUAL_SERVICE_EXTRA = "AnnualServiceExtra";

    public static final String SERVICE_TYPE_GENERAL = "general_service";

    public static final String SERVICE_TYPE_ANNUAL = "annual_service";

    public static final String STATUS_TYPE_EXTRA = "StatusTypeExtra";

    public static final String CUSTOMER_DETAILS_EXTRA = "CustomerDetailsExtra";

    public static final String CUSTOMER_MOBILE_EXTRA = "CustomerMobileExtra";

    public static final String SUBSCRIPTION_DETAILS_EXTRA = "SubscriptionDetailsExtra";
}

