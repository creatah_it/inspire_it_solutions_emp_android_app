package com.creatah.inspireitsolutions.employee.app.modal;

import com.google.gson.annotations.SerializedName;

/**
 * Created by karthikeyan on 04-02-2017.
 */

public class LoginResponse extends BaseResponse {

    @SerializedName("emp_details")
    private UserInfo userInfo;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
