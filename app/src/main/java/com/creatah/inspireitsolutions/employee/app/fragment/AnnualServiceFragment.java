package com.creatah.inspireitsolutions.employee.app.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.framework.utils.DeviceManager;
import com.android.network.listener.ServiceResponseListener;
import com.android.theme.fragment.BaseFragment;
import com.android.theme.utils.Constants;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.activity.AnnualServiceDetailsActivity;
import com.creatah.inspireitsolutions.employee.app.activity.ValidateCustomerActivity;
import com.creatah.inspireitsolutions.employee.app.activity.ValidateSubscriptionActivity;
import com.creatah.inspireitsolutions.employee.app.adapter.AnnualServiceAdapter;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.modal.AnnualService;
import com.creatah.inspireitsolutions.employee.app.modal.AnnualServiceListResponse;
import com.creatah.inspireitsolutions.employee.app.modal.AppInfo;
import com.creatah.inspireitsolutions.employee.app.modal.ServiceStatus;
import com.creatah.inspireitsolutions.employee.app.utils.AppConstant;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by creatah1 on 16-06-2017.
 */

public class AnnualServiceFragment extends BaseFragment implements AnnualServiceAdapter.OnServiceListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView serviceRecyclerView;
    private TextView noResultFound;
    private SwipeRefreshLayout swipeRefreshLayout;

    private boolean isService = false;

    private AnnualServiceAdapter serviceAdapter;
    private List<AnnualService> serviceList = new ArrayList<>();

    private AnnualService service;
    private final int CALL_REQUEST_PERMISSION = 8;
    public static int DETAIL_REQ_CODE = 457;
    private int ADD_REQ_CODE = 476;

    private FloatingActionButton addServiceFab;

    private Spinner statusSpinner;
    private Button filterButton;

    private String statusType = null;

    private List<ServiceStatus> serviceStausList = new ArrayList<>();
    private List<String> serviceStatusSTRList = new ArrayList<>();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service_booking, container, false);
        noResultFound = (TextView) view.findViewById(R.id.noResultFound);
        serviceRecyclerView = (RecyclerView) view.findViewById(R.id.serviceRecyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        //  FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        addServiceFab = (FloatingActionButton) view.findViewById(R.id.addService);
        statusSpinner = (Spinner) view.findViewById(R.id.statusSpinner);
        filterButton = (Button) view.findViewById(R.id.filterButton);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        AppInfo appInfo = AppUtils.getAppInfo(getActivity().getApplication());
        if (appInfo != null && appInfo.getStatusList() != null && appInfo.getStatusList().size() > 0) {

            List<ServiceStatus> serviceStausList1 = appInfo.getStatusList();
            if (serviceStausList1.size() > 0) {
                for (ServiceStatus status : serviceStausList1) {
                    if(!status.getType().equals(getString(R.string.completedStatus))) {
                        serviceStausList.add(status);
                    }
                }
            }


            if (serviceStausList.size() > 0) {
                serviceStatusSTRList.add(getString(R.string.statusAll));
                for (ServiceStatus status : serviceStausList) {

                    serviceStatusSTRList.add(status.getType());
                }
                ArrayAdapter<String> modaladapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, serviceStatusSTRList);
                statusSpinner.setAdapter(modaladapter);
                statusSpinner.setOnItemSelectedListener(modalListener);
            }

        }

        serviceRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(false);
                                        getServiceList();
                                    }
                                }
        );

        addServiceFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addIntent = new Intent(getContext(), ValidateSubscriptionActivity.class);
                startActivityForResult(addIntent, ADD_REQ_CODE);
            }
        });
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getServiceList();
            }
        });
    }

    private void getServiceList() {

        if (DeviceManager.isDeviceOffline(getContext())) {
            showAlert(getContext(), getResources().getString(R.string.title_of_data_connection), getResources().getString(R.string.title_of_data_message),
                    getResources().getString(R.string.ok), null, null, null);
            serviceRecyclerView.setVisibility(View.GONE);
            noResultFound.setVisibility(View.VISIBLE);
            noResultFound.setText(getString(R.string.title_of_data_message));
            return;
        }


        if (!isService) {

            isService = true;

            if(!swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
                mainCategoryHandler.sendEmptyMessage(Constants.SHOW_PROGRESS_DIALOG);
            }else {
                swipeRefreshLayout.setRefreshing(true);
            }

            RequestParams requestParams = new RequestParams();
            requestParams.put("employee_id", String.valueOf(AppUtils.getUserInfo(getActivity().getApplication()).getId()));
            requestParams.put("status_id", statusType);


            ServiceManager.getAnnualServiceList(getContext(), requestParams, new ServiceResponseListener<AnnualServiceListResponse>() {
                        @Override
                        public void onSuccess(AnnualServiceListResponse response) {
                            isService = false;

                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            swipeRefreshLayout.setRefreshing(false);
                            if (response != null && !response.isError() && response.getServiceList() != null && response.getServiceList().size() > 0) {

                                noResultFound.setVisibility(View.GONE);
                                serviceRecyclerView.setVisibility(View.VISIBLE);
                                serviceList.clear();
                                serviceList = response.getServiceList();
                                serviceAdapter = new AnnualServiceAdapter(getActivity(), serviceList, AnnualServiceFragment.this);
                                serviceRecyclerView.setAdapter(serviceAdapter);
                            } else {
                                serviceRecyclerView.setVisibility(View.GONE);
                                noResultFound.setVisibility(View.VISIBLE);
                                noResultFound.setText(getString(R.string.noAMCServiceFound));
                            }

                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            swipeRefreshLayout.setRefreshing(false);
                            serviceRecyclerView.setVisibility(View.GONE);
                            noResultFound.setVisibility(View.VISIBLE);
                            noResultFound.setText("Server unavailable");
                        }
                    }

            );
        }

    }

    @Override
    public void onCallMember(AnnualService service) {
        this.service = service;
        onMakeCall();
    }

    @Override
    public void serviceDetails(AnnualService service) {
        Intent detailIntent = new Intent(getContext(), AnnualServiceDetailsActivity.class);
        detailIntent.putExtra(AppConstant.ANNUAL_SERVICE_EXTRA, service);
        startActivityForResult(detailIntent, DETAIL_REQ_CODE);
    }

    public void onMakeCall() {

        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                AppUtils.makeCall(getActivity(), service.getMobileNo());
            } else if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                    showAlert(getContext(), getString(R.string.callpermission), getString(R.string.callpermissiondesc), "Try", Try, "No", No);
                } else {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CALL_REQUEST_PERMISSION);
                }
            }
        } else {
            AppUtils.makeCall(getActivity(), service.getMobileNo());
        }

    }

    DialogInterface.OnClickListener Try = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CALL_REQUEST_PERMISSION);
        }
    };


    DialogInterface.OnClickListener No = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CALL_REQUEST_PERMISSION:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    AppUtils.makeCall(getActivity(), service.getMobileNo());


                }
                break;


        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DETAIL_REQ_CODE && resultCode == Activity.RESULT_OK) {
            getServiceList();
        }

        if (requestCode == ADD_REQ_CODE && resultCode == Activity.RESULT_OK) {
            getServiceList();
        }
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        getServiceList();
    }

    public AdapterView.OnItemSelectedListener modalListener = new AdapterView.OnItemSelectedListener() {


        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position != 0) {
                statusType = String.valueOf(serviceStausList.get(position - 1).getId());
            } else {
                statusType = null;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
}
