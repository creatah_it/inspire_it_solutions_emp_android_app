package com.android.theme.font;

/**
 * Created by svelan on 3/8/2016.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.android.theme.R;


public class CustomButtonLight extends Button {

    public CustomButtonLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public CustomButtonLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);

    }

    public CustomButtonLight(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MyTextView);
            String fontName = a.getString(R.styleable.MyTextView_fontName);
            if (fontName != null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
                setTypeface(myTypeface);
            } else {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + "Bauhaus_lt.ttf");
                setTypeface(myTypeface);
            }
            a.recycle();
        }
    }

}