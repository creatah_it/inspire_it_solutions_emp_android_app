package com.creatah.inspireitsolutions.employee.app.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.framework.utils.DeviceManager;
import com.android.framework.utils.Utils;
import com.android.network.listener.ServiceResponseListener;
import com.android.theme.activity.BaseActivity;
import com.android.theme.utils.Constants;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.modal.Subscription;
import com.creatah.inspireitsolutions.employee.app.modal.SubscriptionValidationResponse;
import com.creatah.inspireitsolutions.employee.app.utils.AppConstant;
import com.loopj.android.http.RequestParams;

/**
 * Created by creatah1 on 07-08-2017.
 */

public class ValidateSubscriptionActivity extends BaseActivity {

    private EditText subscriptionEditText;
    private Button submitButton;
    private Context context;
    private boolean isService;
    private int BOOK_SERVICE_REQ_CODE = 746;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_subscription);
        setTitle(getString(R.string.validateSubscriptionTitle));
        initializeViews();
        context = this;
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateData();
            }
        });
    }

    private void validateData() {
        String subscripitonId = subscriptionEditText.getText().toString();
        if(TextUtils.isEmpty(subscripitonId)){
            subscriptionEditText.setError(getString(R.string.subscriptionId));
        }else {
            validateSubscription(subscripitonId);
        }
    }

    private void validateSubscription(String subscripitonId) {

        if (DeviceManager.isDeviceOffline(context)) {

            showAlert(context, getResources().getString(R.string.title_of_data_connection), getResources().getString(R.string.title_of_data_message),
                    getResources().getString(R.string.ok), null, null, null);
            return;
        }

        RequestParams requestParams = new RequestParams();
        requestParams.put("subscription_id", subscripitonId);

        if (!isService) {
            mainCategoryHandler.sendEmptyMessage(Constants.SHOW_PROGRESS_DIALOG);
            isService = true;
            ServiceManager.checkValidSubscriptionId(context, requestParams, new ServiceResponseListener<SubscriptionValidationResponse>() {
                        @Override
                        public void onSuccess(SubscriptionValidationResponse response) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);

                            if (response != null && !response.isError()) {
                                Subscription subscription = response.getAnnualSubscription();
                                Intent bookIntent = new Intent(context, CreateAMCServiceActivity.class);
                                bookIntent.putExtra(AppConstant.SUBSCRIPTION_DETAILS_EXTRA, subscription);
                                startActivityForResult(bookIntent, BOOK_SERVICE_REQ_CODE);
                            } else if (response != null && response.isError()) {
                                String message = response.getMessage();
                                Utils.displayToast(context, message, Toast.LENGTH_SHORT);
                            }else {
                                Utils.displayToast(context, getString(R.string.server_unavailable), Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            showAlert(context, getResources().getString(R.string.server_failed), getResources().getString(R.string.server_unavailable),
                                    getResources().getString(R.string.ok), null, null, null);
                        }
                    }

            );
        }


    }


    private void initializeViews() {
        subscriptionEditText = (EditText) findViewById(R.id.subscriptionEditText);
        submitButton = (Button) findViewById(R.id.submitButton);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == BOOK_SERVICE_REQ_CODE && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

}
