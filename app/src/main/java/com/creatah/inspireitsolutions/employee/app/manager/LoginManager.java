package com.creatah.inspireitsolutions.employee.app.manager;

/**
 * Created by karthikeyan on 06-06-2016.
 */

import android.app.Application;


import com.creatah.inspireitsolutions.employee.app.modal.AppInfo;
import com.creatah.inspireitsolutions.employee.app.modal.UserInfo;

import java.io.Serializable;


public class LoginManager implements Serializable {
    private static final long serialVersionUID = 1L;
    private static LoginManager loginManager;
    private UserInfo userInfo;
    private AppInfo appInfo;

    public static LoginManager getInstance() {
        if (loginManager == null) {
            loginManager = new LoginManager();
        }
        return loginManager;
    }

    public void setLogoutDetails(Application application) {

        SharedPreferenceManager.setPreferenceObject(application, SharedPreferenceManager.Keys.USERINFO, null);
        this.userInfo = null;
    }


    public boolean isAuthenticated(Application application) {
        userInfo = SharedPreferenceManager.getPreferenceObject(application, SharedPreferenceManager.Keys.USERINFO);
        if (userInfo != null) {
            return true;
        } else {
            return false;
        }
    }

    public UserInfo getUserInfo(Application application) {

        if (userInfo == null) {
            userInfo = SharedPreferenceManager.getPreferenceObject(application, SharedPreferenceManager.Keys.USERINFO);
        }

        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo, Application application) {
        SharedPreferenceManager.setPreferenceObject(application, SharedPreferenceManager.Keys.USERINFO, userInfo);
        this.userInfo = userInfo;
    }


    public AppInfo getAppInfo(Application application) {

        if (appInfo == null) {
            appInfo = SharedPreferenceManager.getPreferenceObject(application, SharedPreferenceManager.Keys.APPINFO);
        }

        return appInfo;
    }

    public void setAppInfo(AppInfo appInfo, Application application) {
        SharedPreferenceManager.setPreferenceObject(
                application,
                SharedPreferenceManager.Keys.APPINFO,
                appInfo);
        this.appInfo = appInfo;
    }

}
