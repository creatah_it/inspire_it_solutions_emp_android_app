package com.creatah.inspireitsolutions.employee.app.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.theme.activity.BaseActivity;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.adapter.HomePageAdapter;
import com.creatah.inspireitsolutions.employee.app.adapter.NavigationAdapter;
import com.creatah.inspireitsolutions.employee.app.modal.UserInfo;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;

/**
 * Created by creatah1 on 22-05-2017.
 */

public class HomeActivity extends BaseActivity {

    private NavigationAdapter navDrawerAdapter;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mDrawerListView;
    private Toolbar navigationToolbar;

    private UserInfo userInfo;
    private TextView userNameHeaderView;
    private TextView branchTextView;

    private Context context;
    private ImageView navImageView;
    private LinearLayout logoutLayout;

    private int COMPLETED_REQ_CODE = 789;

    private TabLayout tabs;
    private ViewPager pager;
    private HomePageAdapter pagerAdapter;
    CharSequence Titles[] = {"User Bookings", "Annual Service Booking"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        hideToolbar();
        context = HomeActivity.this;
        navigationToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(navigationToolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerListView = (ListView) findViewById(R.id.navListView);
        mDrawerListView.setOnItemClickListener(drawerOnItemClickListener);
        View headerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.nav_header, null, false);
        userNameHeaderView = (TextView) headerView.findViewById(R.id.userNameHeaderView);
        branchTextView = (TextView) headerView.findViewById(R.id.branchHeaderView);
        logoutLayout = (LinearLayout) findViewById(R.id.logoutLayout);

        mDrawerListView.addHeaderView(headerView);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                navigationToolbar, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            @SuppressLint("NewApi")
            public void onDrawerClosed(View view) {
                supportInvalidateOptionsMenu();
            }

            @SuppressLint("NewApi")
            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(false);
        // mDrawerToggle.setHomeAsUpIndicator(R.drawable.home_nav_icon);
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerVisible(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        navigationToolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });

        logoutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.setLogoutDeatails(getApplication());
                startActivity(new Intent(context, LoginActivity.class));
                finish();
            }
        });

        navImageView = (ImageView) findViewById(R.id.navImage);
        navImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });

        initializeSettingsDrawer();

        tabs = (TabLayout) findViewById(R.id.tabs);
        pager = (ViewPager) findViewById(R.id.profileViewPager);
        setUpTabs();
    }


    void setUpTabs() {
        pagerAdapter = new HomePageAdapter(getSupportFragmentManager(), Titles, Titles.length);
        pager.setAdapter(pagerAdapter);
        tabs.setupWithViewPager(pager);
        setupTabIcons();
    }

    private void initializeSettingsDrawer() {
        navDrawerAdapter = new NavigationAdapter(this);
        mDrawerListView.setAdapter(navDrawerAdapter);
        userInfo = AppUtils.getUserInfo(getApplication());
        if (AppUtils.isAuthenticatedUserInfo(getApplication())) {
            userNameHeaderView.setText(userInfo.getName());
            branchTextView.setText(userInfo.getBranchName());
        } else {
            userNameHeaderView.setText(getString(R.string.login));
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private AdapterView.OnItemClickListener drawerOnItemClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> adapter, View arg1, int pos,
                                long arg3) {
            mDrawerLayout.closeDrawers();

            switch (pos) {

                case 0:

                    break;

                case 1:
                    startActivity(new Intent(context, MemberListActivity.class));
                    break;

                case 2:
                    startActivityForResult(new Intent(context, CompletedBookingActivity.class), COMPLETED_REQ_CODE);
                    break;

                case 3:
                    break;


                case 4:

                    break;

                case 5:
                    break;
                case 6:
                    AppUtils.setLogoutDeatails(getApplication());
                    startActivity(new Intent(context, LoginActivity.class));
                    finish();
                    break;
                default:
                    break;
            }

        }
    };

    private void setupTabIcons() {
        // tabs.getTabAt(0).setIcon(R.mipmap.ic_launcher);
        // tabs.getTabAt(1).setIcon(R.mipmap.ic_launcher);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == COMPLETED_REQ_CODE && resultCode == RESULT_OK){
            pagerAdapter = null;
            setUpTabs();
        }

    }

    @Override
    public void onBackPressed() {
        if(mDrawerLayout.isDrawerOpen(Gravity.RIGHT)){
            mDrawerLayout.closeDrawer(Gravity.RIGHT);
        }else {
            finish();
        }
    }
}
