package com.creatah.inspireitsolutions.employee.app.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.theme.activity.BaseActivity;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.fragment.AnnualServiceFragment;
import com.creatah.inspireitsolutions.employee.app.fragment.ServiceFragment;
import com.creatah.inspireitsolutions.employee.app.modal.AnnualService;
import com.creatah.inspireitsolutions.employee.app.modal.InspireService;
import com.creatah.inspireitsolutions.employee.app.utils.AppConstant;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;

/**
 * Created by creatah1 on 07-06-2017.
 */

public class AnnualServiceDetailsActivity extends BaseActivity implements View.OnClickListener {


    private TextView nameTextView;
    private TextView emailTextView;
    private TextView phoneTextView;
    private TextView serviceTextView;
    private TextView serviceTypeTextView;
    private TextView validityTextView;

    private TextView descriptionTextView;
    private TextView amountTextView;
    private TextView sDateTextView;
    private TextView eDateTextView;
    private TextView addressTextView;
    private TextView customerIdTextView;
    private TextView dateTextView;

    private Context context;
    private AnnualService service;

    private boolean isService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_annual_service_details);
        setTitle(getString(R.string.details));
        context = this;
        initializeViews();
        getBundle();

        if (service != null) {
            customerIdTextView.setText(String.valueOf(service.getCustomerId()));
            nameTextView.setText(service.getCustomerName());
            emailTextView.setText(service.getEmailId());
            phoneTextView.setText(service.getMobileNo());
            serviceTextView.setText(service.getAnnualService());
            //warrantyTextView.setText(service.isWarranty() ? getString(R.string.yes) : getString(R.string.no));
            descriptionTextView.setText(service.getDescription());
            validityTextView.setText(service.getValidityYear() + "Years");
            amountTextView.setText(getString(R.string.rs) + " " +service.getAmount());
            serviceTypeTextView.setText(service.getAnnualServiceType());
            addressTextView.setText(service.getAddress());
            sDateTextView.setText(AppUtils.getDateTime1(service.getStartDate()));
            eDateTextView.setText(AppUtils.getDateTime1(service.getEndDate()));
            dateTextView.setText(AppUtils.getNotificationTime(service.getUpdatedDate()));
        }

        findViewById(R.id.addComment).setOnClickListener(this);
        findViewById(R.id.updateButton).setOnClickListener(this);
    }

    private void getBundle() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null && bundle.containsKey(AppConstant.ANNUAL_SERVICE_EXTRA)) {
            service = (AnnualService) bundle.getSerializable(AppConstant.ANNUAL_SERVICE_EXTRA);
        }

    }

    private void initializeViews() {
        nameTextView = (TextView) findViewById(R.id.nameTextView);
        emailTextView = (TextView) findViewById(R.id.emailTextView);
        phoneTextView = (TextView) findViewById(R.id.mobileTextView);
        serviceTextView = (TextView) findViewById(R.id.serviceNameTextView);
        customerIdTextView = (TextView) findViewById(R.id.customerID);
        validityTextView = (TextView) findViewById(R.id.validityTextView);
        descriptionTextView = (TextView) findViewById(R.id.descriptionTextView);
        amountTextView = (TextView) findViewById(R.id.amountTextView);
        sDateTextView = (TextView) findViewById(R.id.sDateTextView);
        eDateTextView = (TextView) findViewById(R.id.eDateTextView);
        serviceTypeTextView = (TextView) findViewById(R.id.serviceTypeTextView);
        addressTextView = (TextView) findViewById(R.id.addressTextView);
        dateTextView = (TextView) findViewById(R.id.dateTextView);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.addComment:
                Intent commentIntent = new Intent(context, ServiceCommentActivity.class);
                commentIntent.putExtra(AppConstant.ANNUAL_SERVICE_EXTRA, service);
                startActivity(commentIntent);
                break;

            case R.id.updateButton:
                Intent detailIntent = new Intent(context, StatusUpdateActivity.class);
                detailIntent.putExtra(AppConstant.ANNUAL_SERVICE_EXTRA, service);
                startActivityForResult(detailIntent, AnnualServiceFragment.DETAIL_REQ_CODE);
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == AnnualServiceFragment.DETAIL_REQ_CODE && resultCode == RESULT_OK){
            setResult(RESULT_OK);
        }
    }
}
