package com.creatah.inspireitsolutions.employee.app.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.framework.utils.DeviceManager;
import com.android.framework.utils.Utils;
import com.android.network.listener.ServiceResponseListener;
import com.android.theme.activity.DialogActivity;
import com.android.theme.utils.Constants;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.adapter.CommentAdapter;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.modal.AddCommentResponse;
import com.creatah.inspireitsolutions.employee.app.modal.AnnualService;
import com.creatah.inspireitsolutions.employee.app.modal.CommentList;
import com.creatah.inspireitsolutions.employee.app.modal.InspireService;
import com.creatah.inspireitsolutions.employee.app.utils.AppConstant;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.loopj.android.http.RequestParams;

/**
 * Created by creatah1 on 09-06-2017.
 */

public class ServiceCommentActivity extends DialogActivity {

    private RecyclerView commentRecyclerView;

    private TextView noResultFound;

    private Context context;

    private boolean isService;

    private InspireService service;

    private AnnualService annualService;

    private EditText commentEditText;

    private boolean isGeneralService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
       /* DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);
        getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.MATCH_PARENT);*/
        context = this;
        initializeViews();
        getBundle();
        commentRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        getCommentList();

        ImageView imageView = (ImageView) findViewById(R.id.commentsend);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateDate();
            }
        });
    }

    private void getBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(AppConstant.BOOKING_ID_EXTRA)) {
            service = (InspireService) bundle.getSerializable(AppConstant.BOOKING_ID_EXTRA);
            isGeneralService = true;
        }else  if (bundle != null && bundle.containsKey(AppConstant.ANNUAL_SERVICE_EXTRA)) {
            annualService = (AnnualService) bundle.getSerializable(AppConstant.ANNUAL_SERVICE_EXTRA);
            isGeneralService = false;
        }
    }

    private void validateDate() {
        String commentStr = commentEditText.getText().toString();
        if (TextUtils.isEmpty(commentStr)) {
            Utils.displayToast(context, "Enter Comment", Toast.LENGTH_SHORT);
        } else {
            addComment(commentStr);
        }
    }

    private void getCommentList() {

        if (DeviceManager.isDeviceOffline(context)) {
            noResultFound.setVisibility(View.VISIBLE);
            commentRecyclerView.setVisibility(View.GONE);
            noResultFound.setText(getString(R.string.title_of_data_message));
            return;
        }

        mainCategoryHandler.sendEmptyMessage(com.android.theme.utils.Constants.SHOW_PROGRESS_DIALOG);

        RequestParams requestParams = new RequestParams();
        if(isGeneralService) {
            requestParams.put("service_id", String.valueOf(service.getId()));
            requestParams.put("service_type", AppConstant.SERVICE_TYPE_GENERAL);
        }else {
            requestParams.put("service_id", String.valueOf(annualService.getServiceId()));
            requestParams.put("service_type", AppConstant.SERVICE_TYPE_ANNUAL);
        }

        if (!isService) {

            isService = true;

            ServiceManager.getCommentList(ServiceCommentActivity.this, requestParams, new ServiceResponseListener<CommentList>() {
                        @Override
                        public void onSuccess(CommentList response) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(com.android.theme.utils.Constants.DISMISS_PROGRESS_DIALOG);

                            if (response != null && !response.isError() && response.getCommentList() != null && response.getCommentList().size() > 0) {

                                noResultFound.setVisibility(View.GONE);
                                commentRecyclerView.setVisibility(View.VISIBLE);
                                commentRecyclerView.setAdapter(new CommentAdapter(ServiceCommentActivity.this, response.getCommentList()));

                            } else {
                                commentRecyclerView.setVisibility(View.GONE);
                                noResultFound.setVisibility(View.VISIBLE);
                                noResultFound.setText(getString(R.string.noCommentsFound));
                            }

                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(com.android.theme.utils.Constants.DISMISS_PROGRESS_DIALOG);
                            commentRecyclerView.setVisibility(View.GONE);
                            noResultFound.setVisibility(View.VISIBLE);
                            noResultFound.setText("Server unavailable");
                        }
                    }

            );
        }


    }


    private void addComment(String commentStr) {

        if (DeviceManager.isDeviceOffline(context)) {
            Toast.makeText(context, getResources().getString(R.string.title_of_data_message), Toast.LENGTH_SHORT).show();
            return;
        }

        RequestParams requestParams = new RequestParams();
        if(isGeneralService) {
            requestParams.put("service_type", AppConstant.SERVICE_TYPE_GENERAL);
            requestParams.put("service_id", String.valueOf(service.getId()));
            requestParams.put("status_id", String.valueOf(service.getServiceStatusId()));
        }else {
            requestParams.put("service_type", AppConstant.SERVICE_TYPE_ANNUAL);
            requestParams.put("service_id", String.valueOf(annualService.getServiceId()));
            requestParams.put("status_id", String.valueOf(annualService.getStatusId()));
        }
        requestParams.put("employee_id", String.valueOf(AppUtils.getUserInfo(getApplication()).getId()));
        requestParams.put("comments", commentStr);


        if (!isService) {
            mainCategoryHandler.sendEmptyMessage(Constants.SHOW_PROGRESS_DIALOG);
            isService = true;
            ServiceManager.addComment(context, requestParams, new ServiceResponseListener<AddCommentResponse>() {
                        @Override
                        public void onSuccess(AddCommentResponse response) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);

                            if (response != null && !response.isError()) {
                                commentEditText.setText("");
                                getCommentList();
                            } else if (response != null) {
                                String message = response.getMessage();
                                Utils.displayToast(context, message, Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            Toast.makeText(context, getResources().getString(R.string.title_of_data_message), Toast.LENGTH_SHORT).show();
                        }
                    }

            );
        }

    }

    private void initializeViews() {
        commentRecyclerView = (RecyclerView) findViewById(R.id.commentRecyclerView);
        commentEditText = (EditText) findViewById(R.id.commentEditText);
        noResultFound = (TextView) findViewById(R.id.noResultFound);
    }
}
