package com.creatah.inspireitsolutions.employee.app.receiver;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.creatah.inspireitsolutions.employee.app.service.AppInfoUpdateService;
import com.creatah.inspireitsolutions.employee.app.service.LocationLoggerService;

/**
 * Created by karthikeyan on 13-10-2016.
 */

public class PackageReplacedReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent != null && intent.getAction().equals(Intent.ACTION_MY_PACKAGE_REPLACED)) {

            Intent pushIntent = new Intent(context, LocationLoggerService.class);
            context.startService(pushIntent);

            Intent appConfigIntent = new Intent(context, AppInfoUpdateService.class);
            context.startService(appConfigIntent);
        }
    }
}
