package com.creatah.inspireitsolutions.employee.app.modal;

import com.google.gson.annotations.SerializedName;

/**
 * Created by karthikeyan on 15-12-2016.
 */

public class UserInfo extends BaseResponse {

    @SerializedName("emp_id")
    private int id;

    @SerializedName("emp_name")
    private String name;

    @SerializedName("email_id")
    private String emailId;

    @SerializedName("mobile_no")
    private String mobileNo;

    @SerializedName("address")
    private String address;

    @SerializedName("designation")
    private String designation;

    @SerializedName("qualification")
    private String qualification;

    @SerializedName("experience")
    private String experience;

    @SerializedName("skill")
    private String skill;

    @SerializedName("photo")
    private String image;

    @SerializedName("branch_id")
    private int branchId;

    @SerializedName("branch_name")
    private String branchName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
}
