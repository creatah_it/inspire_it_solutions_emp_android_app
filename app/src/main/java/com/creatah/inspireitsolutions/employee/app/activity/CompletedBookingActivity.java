package com.creatah.inspireitsolutions.employee.app.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;

import com.android.theme.activity.BaseActivity;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.adapter.HistoryPageAdapter;


/**
 * Created by creatah1 on 08-06-2017.
 */

public class CompletedBookingActivity extends BaseActivity {

    private Context context;
    private TabLayout tabs;
    private ViewPager pager;
    private HistoryPageAdapter pagerAdapter;
    CharSequence Titles[] = {"General Bookings", "Annual Service Booking"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_booking);
        setTitle(getString(R.string.completedBooking));
        initializeViews();
        context = this;
        setUpTabs();
    }


    private void initializeViews() {
        tabs = (TabLayout) findViewById(R.id.tabs);
        pager = (ViewPager) findViewById(R.id.profileViewPager);
    }

    void setUpTabs() {
        pagerAdapter = new HistoryPageAdapter(getSupportFragmentManager(), Titles, Titles.length);
        pager.setAdapter(pagerAdapter);
        tabs.setupWithViewPager(pager);
        setupTabIcons();
    }

    private void setupTabIcons() {
        // tabs.getTabAt(0).setIcon(R.mipmap.ic_launcher);
        // tabs.getTabAt(1).setIcon(R.mipmap.ic_launcher);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }


}
