package com.creatah.inspireitsolutions.employee.app.fcm;

import android.util.Log;

import com.android.framework.utils.DeviceManager;
import com.android.framework.utils.Utils;
import com.android.network.listener.ServiceResponseListener;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.modal.UpdateFCMIdResponse;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.loopj.android.http.RequestParams;

/**
 * Created by creatah1 on 19-05-2017.
 */

public class FirebaseIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";
    private boolean isService = false;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
        if (DeviceManager.isDeviceOffline(getApplicationContext())) {
            return;
        }
        if (!isService) {
            isService = true;

            final RequestParams requestParams = new RequestParams();
            requestParams.put("fcm_id", token);
            requestParams.put("device_id", Utils.getAndroidSecureId(getApplicationContext()));
            if (AppUtils.isAuthenticatedUserInfo(getApplication())) {
                requestParams.put("empoyee_id", String.valueOf(AppUtils.getUserInfo(getApplication()).getId()));
            }
            ServiceManager.sendFCMId(FirebaseIDService.this, requestParams, new ServiceResponseListener<UpdateFCMIdResponse>() {
                @Override
                public void onSuccess(UpdateFCMIdResponse response) {
                    isService = false;
                    if (!response.isError()) {

                    } else if (!response.isError()) {

                    }
                }

                @Override
                public void onFailure(Throwable throwable, String errorResponse) {
                    isService = false;
                }
            });
        }
    }
}
