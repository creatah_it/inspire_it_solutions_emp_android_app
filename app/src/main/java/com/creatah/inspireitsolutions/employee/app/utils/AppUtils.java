package com.creatah.inspireitsolutions.employee.app.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ParseException;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;

import com.android.network.listener.ServiceResponseListener;
import com.creatah.inspireitsolutions.employee.app.listener.UiUpdateListener;
import com.creatah.inspireitsolutions.employee.app.manager.LoginManager;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.manager.SharedPreferenceManager;
import com.creatah.inspireitsolutions.employee.app.modal.AppInfo;
import com.creatah.inspireitsolutions.employee.app.modal.EmployeeLocationResponse;
import com.creatah.inspireitsolutions.employee.app.modal.UserInfo;
import com.creatah.inspireitsolutions.employee.app.receiver.AlarmManagerReceiver;
import com.creatah.inspireitsolutions.employee.app.service.LocationLoggerService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.loopj.android.http.RequestParams;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by karthikeyan on 27-07-2016.
 */
public class AppUtils {


    public static AppInfo getAppInfo(Application application) {

        if (LoginManager.getInstance() != null && LoginManager.getInstance().getAppInfo(application) != null) {
            return LoginManager.getInstance().getAppInfo(application);
        }

        return null;
    }

    public static void setAppInfo(AppInfo appInfo, Application application) {
        LoginManager.getInstance().setAppInfo(appInfo, application);
    }


    public static boolean isAuthenticatedAppInfo(Application application) {
        if (LoginManager.getInstance() != null && LoginManager.getInstance().getAppInfo(application) != null) {
            return true;
        } else {
            return false;
        }
    }


    public static void setLogoutDeatails(Application application) {
        LoginManager.getInstance().setLogoutDetails(application);
    }

    public static UserInfo getUserInfo(Application application) {

        if (LoginManager.getInstance() != null && LoginManager.getInstance().getUserInfo(application) != null) {
            return LoginManager.getInstance().getUserInfo(application);
        }

        return null;
    }

    public static void setUserInfo(UserInfo userInfo, Application application) {
        LoginManager.getInstance().setUserInfo(userInfo, application);
    }


    public static boolean isAuthenticatedUserInfo(Application application) {
        if (LoginManager.getInstance() != null && LoginManager.getInstance().getUserInfo(application) != null) {
            return true;
        } else {
            return false;
        }
    }


    public static String getCreatedDateTime(String time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat outputFormatAmPm = new SimpleDateFormat("HH:mm:ss");
        try {
            Date date = simpleDateFormat.parse(time);
            return outputFormatAmPm.format(date);
        } catch (ParseException e) {
            return "Error parsing date";
        } catch (java.text.ParseException e) {
            return "Error parsing date";
        }
    }

    public static void ShareIntentMessage(Activity context, String message) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendIntent.setType("text/plain");
        sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(Intent.createChooser(sendIntent, "Share using"));
    }

    public static String getDuration(String date) {
        String time = date.substring(2);
        long duration = 0L;
        Object[][] indexs = new Object[][]{{"H", 3600}, {"M", 60}, {"S", 1}};
        for (int i = 0; i < indexs.length; i++) {
            int index = time.indexOf((String) indexs[i][0]);
            if (index != -1) {
                String value = time.substring(0, index);
                duration += Integer.parseInt(value) * (int) indexs[i][1] * 1000;
                time = time.substring(value.length() + 1);
            }
        }

        if (TimeUnit.MILLISECONDS.toHours(duration) == 0) {
            return String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(duration) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duration)), // The change is in this line
                    TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
        } else {


            return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(duration), TimeUnit.MILLISECONDS.toMinutes(duration) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duration)), // The change is in this line
                    TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
        }
    }

    public static String convertstringTodate1(String date_String) {
        SimpleDateFormat inputFormat24 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //SimpleDateFormat outputFormatAmPm = new SimpleDateFormat("MMM'\n'dd'\n'EEEE'\n'KK:mm:ss a");
        SimpleDateFormat outputFormatAmPm = new SimpleDateFormat("dd MMM" + " " + "yyyy hh:mm a");
        try {
            Date date = null;
            try {
                date = inputFormat24.parse(date_String);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            return outputFormatAmPm.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void setAppInfoAlarmManager(Context context) {
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmManagerReceiver.class);

        PendingIntent morningAlarmIntent = PendingIntent.getBroadcast(context, AppConstant.MORNING_ALARM_REQ_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar now = Calendar.getInstance();

        Calendar morningAlarmStartTime = Calendar.getInstance();
        morningAlarmStartTime.set(Calendar.HOUR_OF_DAY, 9);
        morningAlarmStartTime.set(Calendar.MINUTE, 00);
        morningAlarmStartTime.set(Calendar.SECOND, 00);

        if (now.after(morningAlarmStartTime)) {
            morningAlarmStartTime.add(Calendar.DATE, 1);
        }

        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, morningAlarmStartTime.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, morningAlarmIntent);

        SharedPreferenceManager.savePreferences(context, SharedPreferenceManager.Keys.IS_ALARM_SET, true);

    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static String rupeeFormat(String value) {
        value = value.replace(",", "");
        char lastDigit = value.charAt(value.length() - 1);
        String result = "";
        int len = value.length() - 1;
        int nDigits = 0;

        for (int i = len - 1; i >= 0; i--) {
            result = value.charAt(i) + result;
            nDigits++;
            if (((nDigits % 2) == 0) && (i > 0)) {
                result = "," + result;
            }
        }
        return (result + lastDigit);
    }

    public static void makeCall(Activity activity, String phoneNo) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phoneNo));
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        activity.startActivity(intent);
    }

    public static String getNotificationTime(String date_String) {
        SimpleDateFormat inputFormat24 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //SimpleDateFormat outputFormatAmPm = new SimpleDateFormat("MMM'\n'dd'\n'EEEE'\n'KK:mm:ss a");
        SimpleDateFormat outputFormatAmPm = new SimpleDateFormat(" dd MMM yy, hh:mm a");
        try {
            Date date = null;
            try {
                date = inputFormat24.parse(date_String);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            return outputFormatAmPm.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getDateTime1(String date_String) {
        SimpleDateFormat inputFormat24 = new SimpleDateFormat("yyyy-MM-dd");
        //SimpleDateFormat outputFormatAmPm = new SimpleDateFormat("MMM'\n'dd'\n'EEEE'\n'KK:mm:ss a");
        SimpleDateFormat outputFormatAmPm = new SimpleDateFormat(" dd MMM yy");
        try {
            Date date = null;
            try {
                date = inputFormat24.parse(date_String);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            return outputFormatAmPm.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }



    public static void updateLocation(Context context, RequestParams requestParams, final UiUpdateListener uiUpdateListener) {
        ServiceManager.updateEmployeeLocation(context, requestParams,
                new ServiceResponseListener<EmployeeLocationResponse>() {
                    @Override
                    public void onSuccess(EmployeeLocationResponse response) {
                        if (!(uiUpdateListener == null)) {
                            uiUpdateListener.OnUpdateUI(response);
                        }
                        Log.i("loc update", "location updated to server successfully");
                    }

                    @Override
                    public void onFailure(Throwable throwable, String errorResponse) {
                        if (!(uiUpdateListener == null)) {
                            uiUpdateListener.OnUpdateUI(errorResponse);
                        }
                        Log.i("loc update", "location updated to server failed");
                    }
                });

    }

    public static void startLocationService(Context context){
        context.startService(new Intent(context,
                LocationLoggerService.class));
        SharedPreferenceManager.savePreferences(context, SharedPreferenceManager.Keys.IS_LOCATION_SERVICE, true);
    }

    public static boolean checkPlayServices(Context context) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }
}
