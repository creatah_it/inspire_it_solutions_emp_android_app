package com.creatah.inspireitsolutions.employee.app.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.framework.utils.Utils;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.manager.ImageLoader;
import com.creatah.inspireitsolutions.employee.app.modal.Member;

import java.util.List;

/**
 * Created by creatah1 on 03-06-2017.
 */

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.MemberViewHolder> {

    private Activity activity;
    private List<Member> memberList;
    private ImageLoader imageLoader;
    private OnMemberListener listener;

    public MemberAdapter(Activity activity, List<Member> memberList, OnMemberListener listener) {
        this.activity = activity;
        this.memberList = memberList;
        this.listener = listener;
        imageLoader = new ImageLoader(activity, R.drawable.loading_image, R.drawable.no_image);
    }

    @Override
    public MemberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_layout_member, parent, false);
        MemberViewHolder viewHolder = new MemberViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MemberViewHolder holder, int position) {
        final Member member = memberList.get(position);

        holder.nameTextView.setText(member.getName());
        holder.mobileTextView.setText(member.getMobile());
        holder.designationTextView.setText(member.getDesignation());

        if (member.getImage() != null) {
            String imageUrl = Utils.getBaseImageUrl(activity) + member.getImage();
            imageLoader.displayImage(imageUrl, holder.memberImageView);
        } else {
            holder.memberImageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.no_image));
        }

        holder.callLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onCallMember(member);
            }
        });

    }

    @Override
    public int getItemCount() {
        return memberList.size();
    }


    public class MemberViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private TextView designationTextView;
        private TextView mobileTextView;

        private ImageView memberImageView;

        private LinearLayout callLayout;

        public MemberViewHolder(View itemView) {
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.nameTextView);
            designationTextView = (TextView) itemView.findViewById(R.id.designationTextView);
            mobileTextView = (TextView) itemView.findViewById(R.id.mobileTextView);

            memberImageView = (ImageView) itemView.findViewById(R.id.memberImage);
            callLayout = (LinearLayout) itemView.findViewById(R.id.callLayout);

        }
    }

    public interface OnMemberListener {
        void onCallMember(Member member);
    }

}
