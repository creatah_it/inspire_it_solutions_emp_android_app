package com.creatah.inspireitsolutions.employee.app.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.theme.activity.BaseActivity;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.fragment.ServiceFragment;
import com.creatah.inspireitsolutions.employee.app.modal.InspireService;
import com.creatah.inspireitsolutions.employee.app.utils.AppConstant;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;

/**
 * Created by creatah1 on 07-06-2017.
 */

public class ServiceDetailsActivity extends BaseActivity implements View.OnClickListener {


    private TextView nameTextView;
    private TextView emailTextView;
    private TextView phoneTextView;
    private TextView deviceTextView;
    private TextView brandTextView;
    private TextView modelTextView;
    private TextView serviceTypeTextView;
    private TextView warrantyTextView;
    private TextView serialTextView;
    private TextView descriptionTextView;
    private TextView scheduleTextView;
    private TextView engagementTextView;
    private TextView addressTextView;
    private TextView customerIdTextView;
    private TextView dateTextView;

    private Context context;
    private InspireService service;

    private boolean isService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_details);
        setTitle(getString(R.string.details));
        context = this;
        initializeViews();
        getBundle();

        if (service != null) {
            customerIdTextView.setText(String.valueOf(service.getCustomerId()));
            nameTextView.setText(service.getCustomerName());
            emailTextView.setText(service.getEmailId());
            phoneTextView.setText(service.getMobileNo());
            deviceTextView.setText(service.getServiceName());
            brandTextView.setText(service.getBrand());
            modelTextView.setText(service.getModel());
            //warrantyTextView.setText(service.isWarranty() ? getString(R.string.yes) : getString(R.string.no));
            serialTextView.setText(service.getSerialNo());
            descriptionTextView.setText(service.getDescription());
            scheduleTextView.setText(service.getPreferredDate());
            engagementTextView.setText(service.getEngagementType());
            serviceTypeTextView.setText(service.getServiceTypeName());
            addressTextView.setText(service.getAddress());
            dateTextView.setText(AppUtils.getNotificationTime(service.getUpdatedDate()));
        }

        findViewById(R.id.addComment).setOnClickListener(this);
        findViewById(R.id.updateButton).setOnClickListener(this);
    }

    private void getBundle() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null && bundle.containsKey(AppConstant.SERVICE_DETAILS_EXTRA)) {
            service = (InspireService) bundle.getSerializable(AppConstant.SERVICE_DETAILS_EXTRA);
        }

    }

    private void initializeViews() {
        nameTextView = (TextView) findViewById(R.id.nameTextView);
        emailTextView = (TextView) findViewById(R.id.emailTextView);
        phoneTextView = (TextView) findViewById(R.id.mobileTextView);
        deviceTextView = (TextView) findViewById(R.id.deviceTextView);
        brandTextView = (TextView) findViewById(R.id.brandTextView);
        modelTextView = (TextView) findViewById(R.id.modalTextView);
        customerIdTextView = (TextView) findViewById(R.id.customerID);
        warrantyTextView = (TextView) findViewById(R.id.warrantyTextView);
        serialTextView = (TextView) findViewById(R.id.serialTextView);
        descriptionTextView = (TextView) findViewById(R.id.descriptionTextView);
        scheduleTextView = (TextView) findViewById(R.id.scheduleTextView);
        engagementTextView = (TextView) findViewById(R.id.engagementTextView);
        serviceTypeTextView = (TextView) findViewById(R.id.serviceTypeTextView);
        addressTextView = (TextView) findViewById(R.id.addressTextView);
        dateTextView = (TextView) findViewById(R.id.dateTextView);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.addComment:
                Intent commentIntent = new Intent(context, ServiceCommentActivity.class);
                commentIntent.putExtra(AppConstant.BOOKING_ID_EXTRA, service);
                startActivity(commentIntent);
                break;

            case R.id.updateButton:
                Intent detailIntent = new Intent(context, StatusUpdateActivity.class);
                detailIntent.putExtra(AppConstant.SERVICE_DETAILS_EXTRA, service);
                startActivityForResult(detailIntent, ServiceFragment.DETAIL_REQ_CODE);
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ServiceFragment.DETAIL_REQ_CODE && resultCode == RESULT_OK){
            setResult(RESULT_OK);
        }
    }
}
