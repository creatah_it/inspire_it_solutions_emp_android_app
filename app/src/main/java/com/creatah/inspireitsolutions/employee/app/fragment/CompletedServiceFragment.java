package com.creatah.inspireitsolutions.employee.app.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.framework.utils.DeviceManager;
import com.android.network.listener.ServiceResponseListener;
import com.android.theme.fragment.BaseFragment;
import com.android.theme.utils.Constants;
import com.creatah.inspireitsolutions.employee.app.R;
import com.creatah.inspireitsolutions.employee.app.activity.ServiceDetailsActivity;
import com.creatah.inspireitsolutions.employee.app.adapter.CompletedServiceAdapter;
import com.creatah.inspireitsolutions.employee.app.adapter.ServiceAdapter;
import com.creatah.inspireitsolutions.employee.app.manager.ServiceManager;
import com.creatah.inspireitsolutions.employee.app.modal.AppInfo;
import com.creatah.inspireitsolutions.employee.app.modal.InspireService;
import com.creatah.inspireitsolutions.employee.app.modal.ServiceListResponse;
import com.creatah.inspireitsolutions.employee.app.modal.ServiceStatus;
import com.creatah.inspireitsolutions.employee.app.utils.AppConstant;
import com.creatah.inspireitsolutions.employee.app.utils.AppUtils;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by creatah1 on 16-06-2017.
 */

public class CompletedServiceFragment extends BaseFragment implements CompletedServiceAdapter.OnServiceListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView serviceRecyclerView;
    private TextView noResultFound;
    private SwipeRefreshLayout swipeRefreshLayout;

    private boolean isService = false;

    private CompletedServiceAdapter serviceAdapter;
    private List<InspireService> serviceList = new ArrayList<>();

    private InspireService service;
    private final int CALL_REQUEST_PERMISSION = 7;
    public static int DETAIL_REQ_CODE = 456;
    private String statusId = null;

    private CompletedServiceFragment newInstance(String status){
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstant.STATUS_TYPE_EXTRA, status);
        CompletedServiceFragment fragment = new CompletedServiceFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public CompletedServiceFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* Bundle args = getArguments();
        if(args!=null && args.containsKey(AppConstant.STATUS_TYPE_EXTRA)) {
            statusId =args.getString(AppConstant.STATUS_TYPE_EXTRA);
        }*/

        AppInfo appInfo = AppUtils.getAppInfo(getActivity().getApplication());
        if (appInfo != null && appInfo.getStatusList() != null && appInfo.getStatusList().size() > 0) {

            List<ServiceStatus> serviceStausList = appInfo.getStatusList();
            if (serviceStausList.size() > 0) {
                for (ServiceStatus status : serviceStausList) {
                    if(status.getType().equals(getString(R.string.completedStatus))){
                        statusId = String.valueOf(status.getId());
                    }
                }

            }

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_completed_service_booking, container, false);
        noResultFound = (TextView) view.findViewById(R.id.noResultFound);
        serviceRecyclerView = (RecyclerView) view.findViewById(R.id.serviceRecyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        view.findViewById(R.id.addService).setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        serviceRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(false);
                                        getServiceList();
                                    }
                                }
        );
    }

    private void getServiceList() {

        if (DeviceManager.isDeviceOffline(getContext())) {
            showAlert(getContext(), getResources().getString(R.string.title_of_data_connection), getResources().getString(R.string.title_of_data_message),
                    getResources().getString(R.string.ok), null, null, null);
            serviceRecyclerView.setVisibility(View.GONE);
            noResultFound.setVisibility(View.VISIBLE);
            noResultFound.setText(getString(R.string.title_of_data_message));
            return;
        }


        if (!isService) {

            isService = true;

            if(!swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
                mainCategoryHandler.sendEmptyMessage(Constants.SHOW_PROGRESS_DIALOG);
            }else {
                swipeRefreshLayout.setRefreshing(true);
            }


            RequestParams requestParams = new RequestParams();
            requestParams.put("emp_id", String.valueOf(AppUtils.getUserInfo(getActivity().getApplication()).getId()));
            requestParams.put("status_id", statusId);

            ServiceManager.getServiceList(getContext(), requestParams, new ServiceResponseListener<ServiceListResponse>() {
                        @Override
                        public void onSuccess(ServiceListResponse response) {
                            isService = false;

                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            swipeRefreshLayout.setRefreshing(false);
                            if (response != null && !response.isError() && response.getServiceList() != null && response.getServiceList().size() > 0) {

                                noResultFound.setVisibility(View.GONE);
                                serviceRecyclerView.setVisibility(View.VISIBLE);
                                serviceList.clear();
                                serviceList = response.getServiceList();
                                serviceAdapter = new CompletedServiceAdapter(getActivity(), serviceList, CompletedServiceFragment.this);
                                serviceRecyclerView.setAdapter(serviceAdapter);
                            } else {
                                serviceRecyclerView.setVisibility(View.GONE);
                                noResultFound.setVisibility(View.VISIBLE);
                                noResultFound.setText("No Service Found");
                            }

                        }

                        @Override
                        public void onFailure(Throwable throwable, String errorResponse) {
                            isService = false;
                            mainCategoryHandler.sendEmptyMessage(Constants.DISMISS_PROGRESS_DIALOG);
                            swipeRefreshLayout.setRefreshing(false);
                            serviceRecyclerView.setVisibility(View.GONE);
                            noResultFound.setVisibility(View.VISIBLE);
                            noResultFound.setText("Server unavailable");
                        }
                    }

            );
        }

    }

    @Override
    public void onCallMember(InspireService service) {
        this.service = service;
        onMakeCall();
    }

    @Override
    public void serviceDetail(InspireService service) {
        Intent detailIntent = new Intent(getContext(), ServiceDetailsActivity.class);
        detailIntent.putExtra(AppConstant.SERVICE_DETAILS_EXTRA, service);
        startActivityForResult(detailIntent, DETAIL_REQ_CODE);
    }

    public void onMakeCall() {

        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                AppUtils.makeCall(getActivity(), service.getMobileNo());
            } else if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                    showAlert(getContext(), getString(R.string.callpermission), getString(R.string.callpermissiondesc), "Try", Try, "No", No);
                } else {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CALL_REQUEST_PERMISSION);
                }
            }
        } else {
            AppUtils.makeCall(getActivity(), service.getMobileNo());
        }

    }

    DialogInterface.OnClickListener Try = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CALL_REQUEST_PERMISSION);
        }
    };


    DialogInterface.OnClickListener No = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CALL_REQUEST_PERMISSION:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    AppUtils.makeCall(getActivity(), service.getMobileNo());


                }
                break;


        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DETAIL_REQ_CODE && resultCode == Activity.RESULT_OK) {
            getServiceList();
            getActivity().setResult(Activity.RESULT_OK);
        }
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        getServiceList();
    }
}
