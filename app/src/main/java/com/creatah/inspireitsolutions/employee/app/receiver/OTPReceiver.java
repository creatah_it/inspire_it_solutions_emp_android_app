package com.creatah.inspireitsolutions.employee.app.receiver;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;


/**
 * Created by karthikeyan on 22-07-2016.
 */
public class OTPReceiver extends BroadcastReceiver {

    public static final String SMS_BUNDLE = "pdus";
    String smsBody;
    String address;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCheck = ContextCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.READ_SMS);
            if (permissionCheck != 0) {
                return;
            }
        }

        Bundle intentExtras = intent.getExtras();
        if (intentExtras != null) {
            Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);
            String smsMessageStr = "";
            for (int i = 0; i < sms.length; ++i) {
                android.telephony.SmsMessage smsMessage = android.telephony.SmsMessage.createFromPdu((byte[]) sms[i]);

                smsBody = smsMessage.getMessageBody().toString();
                address = smsMessage.getOriginatingAddress();
                smsMessageStr += "SMS From: " + address + "\n";
                smsMessageStr += smsBody + "\n";
            }
           /* OtpActivity inst = OtpActivity.instance();
            if (inst != null) {
                try {
                    // if(address.equalsIgnoreCase("040060")) {
                    inst.updateList(smsBody);
                    // }
                } catch (Exception e) {
                    e.printStackTrace();
                    String s = String.valueOf(e);
                    Log.d("", s);
                }
            }

            ForgotPasswordOtpActivity inst1 = ForgotPasswordOtpActivity.instance();
            if(inst1!=null)
            {
                try {
                    // if(address.equalsIgnoreCase("040060")) {
                    inst1.updateList(smsBody);
                    // }
                }catch (Exception e)
                {
                    e.printStackTrace();
                    String s= String.valueOf(e);
                    Log.d("", s);
                }
            }*/
        }

    }


}
